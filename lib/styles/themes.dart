import 'package:flutter/material.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class Themes {
  static ThemeData darkTheme = ThemeData(
    fontFamily: "Noto",
    colorScheme: ColorScheme.dark(
      primary: Constants.darkPrimaryColor,
      secondary: Constants.darkSecondaryColor,
      background: const Color(0xff121212),
      surface: const Color(0xff202020),
    ),
    scaffoldBackgroundColor: const Color(0xff121212),
    cardColor: const Color(0xff202020),
    appBarTheme: const AppBarTheme(elevation: 0),
  );
  static ThemeData lightTheme = ThemeData(
    fontFamily: "Noto",
    colorScheme: ColorScheme.light(
      primary: Constants.lightPrimaryColor,
      secondary: Constants.lightSecondaryColor,
    ),
    appBarTheme: const AppBarTheme(elevation: 0),
  );
}
