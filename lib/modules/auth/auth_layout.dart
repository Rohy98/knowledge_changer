import 'dart:async';

import 'package:flutter/material.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class AuthLayout extends StatefulWidget {
  const AuthLayout({
    Key? key,
    required this.boxContent,
    required this.actions,
    this.showBackgroundFromTopSide = true,
  }) : super(key: key);

  final Widget boxContent;
  final List<Widget> actions;
  final bool showBackgroundFromTopSide;

  @override
  _AuthLayoutState createState() => _AuthLayoutState();
}

class _AuthLayoutState extends State<AuthLayout> {
  late bool beginAnimation;
  late bool showBackgroundAnimation;

  @override
  void initState() {
    beginAnimation = false;
    showBackgroundAnimation = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var animationDuration = const Duration(milliseconds: 300);
    var animationCurve = Curves.easeInOutQuart;
    if (!beginAnimation) {
      Timer(const Duration(milliseconds: 20), () {
        setState(() {
          showBackgroundAnimation = true;
        });
      });
      Timer(const Duration(milliseconds: 2000), () {
        setState(() {
          beginAnimation = true;
        });
      });
    }
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Stack(
        children: [
          AnimatedPositioned(
            top: !widget.showBackgroundFromTopSide ? (showBackgroundAnimation ? 0 : screenSize.height) : null,
            bottom: widget.showBackgroundFromTopSide ? (showBackgroundAnimation ? 0 : screenSize.height) : null,
            left: 0,
            duration: const Duration(seconds: 1),
            curve: animationCurve,
            child: AnimatedOpacity(
              opacity: showBackgroundAnimation ? 1 : 0.5,
              duration: const Duration(milliseconds: 700),
              curve: animationCurve,
              child: Image.asset(
                "assets/images/sky.jpg",
                height: screenSize.height,
                width: screenSize.width,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.spacing * 3),
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AnimatedScale(
                          alignment: Alignment.topCenter,
                          scale: beginAnimation ? 1 : 0,
                          duration: animationDuration,
                          curve: animationCurve,
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.95),
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: const [
                                BoxShadow(
                                  blurRadius: 7,
                                  offset: Offset(1, 1),
                                  color: Colors.black26,
                                )
                              ],
                            ),
                            child: widget.boxContent,
                          ),
                        ),
                        SizedBox(height: Constants.spacing * 2),
                        for (var i = 0; i < widget.actions.length; i++)
                          AnimatedScale(
                            alignment: i.isEven ? Alignment.centerRight : Alignment.centerLeft,
                            scale: beginAnimation ? 1 : 0,
                            duration: animationDuration,
                            curve: animationCurve,
                            child: widget.actions[i],
                          ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
