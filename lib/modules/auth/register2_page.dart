import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:knowledge_changer/components/avatar_image_picker.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/drop_down_search_component.dart';
import 'package:knowledge_changer/enums/alert_type.dart';
import 'package:knowledge_changer/models/user_model.dart';
import 'package:knowledge_changer/modules/auth/auth_layout.dart';
import 'package:knowledge_changer/modules/auth/login_page.dart';
import 'package:knowledge_changer/modules/shared/shared_layout.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart' as intl;

class RegisterPage2 extends StatefulWidget {
  const RegisterPage2({Key? key}) : super(key: key);

  @override
  State<RegisterPage2> createState() => _RegisterPage2State();
}

class _RegisterPage2State extends State<RegisterPage2> {
  bool gender = false;
  DateTime birthDate = DateTime(2000);
  final TextEditingController lastNameController = TextEditingController();
  String country = "المملكة العربية السعودية";
  XFile? pickedProfileImage;

  bool isLoading = false;
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    return AuthLayout(
      showBackgroundFromTopSide: false,
      boxContent: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            "استكمال معلومات حسابك",
            style: TextStyle(fontSize: 25),
          ),
          SizedBox(height: Constants.spacing),
          AvatarImagePicker(
            onNetwork: false,
            image: pickedProfileImage != null ? File(pickedProfileImage!.path) : null,
            onDeletePhoto: () {
              setState(() {
                pickedProfileImage = null;
              });
            },
            onPickedPhoto: (XFile? f) async {
              setState(() {
                pickedProfileImage = f;
              });
            },
          ),
          SizedBox(height: Constants.spacing),
          Form(
            key: formKey,
            child: Column(
              children: [
                InkWell(
                  onTap: () async {
                    final datePick = await showDatePicker(
                      context: context,
                      initialDate: birthDate,
                      firstDate: DateTime(1800),
                      lastDate: DateTime.now().subtract(const Duration(days: 365 * 5)),
                    );
                    if (datePick != null && datePick != birthDate) {
                      setState(() {
                        birthDate = datePick;
                      });
                    }
                  },
                  child: AbsorbPointer(
                    child: TextFormField(
                      controller: TextEditingController(text: intl.DateFormat("yyyy-MM-dd").format(birthDate)),
                      decoration: const InputDecoration(
                        hintText: "تاريخ الميلاد",
                        icon: Icon(Icons.date_range_outlined),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: Constants.spacing),
                DropDownSearchComponent<String>(
                  items: Constants.countries,
                  label: "البلد",
                  popupTitle: "قم باختيار دولتك",
                  selectedItem: country,
                  onChanged: (val) {
                    setState(() {
                      country = val!;
                    });
                  },
                ),
                SizedBox(height: Constants.spacing),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("الجنس"),
                    Row(
                      children: [
                        Expanded(
                          child: RadioListTile<bool?>(
                            title: const Text("ذكر"),
                            value: gender,
                            groupValue: false,
                            onChanged: (v) {
                              setState(() {
                                gender = false;
                              });
                            },
                          ),
                        ),
                        Expanded(
                          child: RadioListTile<bool?>(
                            title: const Text("أنثى"),
                            value: gender,
                            groupValue: true,
                            onChanged: (v) {
                              setState(() {
                                gender = true;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      actions: [
        Visibility(
          visible: isLoading,
          child: Column(
            children: [
              const SizedBox(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              ),
              SizedBox(height: Constants.spacing * 2),
            ],
          ),
        ),
        Visibility(
          visible: !isLoading,
          child: Column(
            children: [
              TextIconButton(
                label: "تم",
                icon: Icons.done,
                onTap: () async {
                  if (formKey.currentState!.validate()) {
                    setState(() {
                      isLoading = true;
                    });
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    late UserModel user;
                    String? userAsString = prefs.getString("user");
                    if (userAsString != null) {
                      user = UserModel.fromJson(jsonDecode(userAsString));
                    }
                    String? fileName = pickedProfileImage != null ? pickedProfileImage!.path.split('/').last : null;
                    FormData formData = FormData.fromMap({
                      "user": user.id,
                      "gender": !gender ? "ذكر" : "انثى",
                      "birthdate": intl.DateFormat("yyyy-MM-dd").format(birthDate),
                      "country": country,
                      "profileImage": fileName != null ? await MultipartFile.fromFile(pickedProfileImage!.path, filename: fileName) : "",
                    });
                    var res = await DioHelper.dio.post("/api/updateUserData", data: formData);
                    AppCubit.get(context).fillUserObjectFromUserInfo(
                      b: res.data["birthdate"],
                      g: res.data["gender"] == "انثى",
                      c: res.data["country"],
                      p: res.data["profileImage"],
                    );
                    AppCubit.get(context).fillUserInfoFromUserObject();

                    prefs.setString("user", jsonEncode(AppCubit.get(context).user.toJson()));
                    setState(() {
                      isLoading = false;
                    });
                    if (res.statusCode == 201 || res.statusCode == 200) {
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const SharedLayout()), (r) => false);
                      SharedUtils.showSnackBar("تم إكمال معلومات حسابك بنجاح", context, alertType: AlertType.success);
                    } else {
                      SharedUtils.showSnackBar("فشلت العملية", context, alertType: AlertType.error);
                    }
                  }
                },
              ),
              SizedBox(height: Constants.spacing * 2),
            ],
          ),
        ),
        TextIconButton(
          label: "تخطي",
          icon: Icons.login,
          color: Colors.deepPurple[300],
          labelColor: colorScheme.onPrimary,
          onTap: () {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const LoginPage()), (r) => false);
          },
        )
      ],
    );
  }
}
