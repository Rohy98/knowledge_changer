import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/text_form_field_component.dart';
import 'package:knowledge_changer/modules/auth/auth_layout.dart';
import 'package:knowledge_changer/modules/auth/register_page.dart';
import 'package:knowledge_changer/modules/shared/shared_layout.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key, this.username}) : super(key: key);

  final String? username;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  void initState() {
    if (widget.username != null) {
      usernameController.text = widget.username!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    return AuthLayout(
      boxContent: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            "تسجيل الدخول",
            style: TextStyle(fontSize: 30),
          ),
          Icon(
            Icons.person,
            size: 130,
            color: colorScheme.primary,
          ),
          Form(
            key: formKey,
            child: Column(
              children: [
                TextFormFieldComponent(
                  label: "اسم المستخدم",
                  controller: usernameController,
                  icon: Icons.person_outline,
                  required: true,
                ),
                SizedBox(height: Constants.spacing),
                TextFormFieldComponent(
                  label: "كلمة المرور",
                  controller: passwordController,
                  icon: Icons.lock_outline,
                  obscureText: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "هذا الحقل مطلوب";
                    } else if (value.length < 8) {
                      return "يرجى إدخال 8 محارف على الأقل";
                    }
                    return null;
                  },
                ),
                SizedBox(height: Constants.spacing * 2),
              ],
            ),
          ),
          MaterialButton(
            onPressed: () {},
            child: const Text("هل نسيت كلمة السر ؟"),
          )
        ],
      ),
      actions: [
        Visibility(
          visible: isLoading,
          child: Column(
            children: [
              const SizedBox(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              ),
              SizedBox(height: Constants.spacing * 2),
            ],
          ),
        ),
        Visibility(
          visible: !isLoading,
          child: Column(
            children: [
              TextIconButton(
                label: "تسجيل الدخول",
                icon: Icons.login,
                onTap: () async {
                  if (formKey.currentState!.validate()) {
                    setState(() {
                      isLoading = true;
                    });
                    if (await SharedUtils.login(usernameController.text, passwordController.text, AppCubit.get(context))) {
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const SharedLayout()), (route) => false);
                    } else {
                      SharedUtils.showSnackBar("خطأ في اسم المستخدم أو كلمة المرور", context);
                    }

                    setState(() {
                      isLoading = false;
                    });
                  }
                },
              ),
              SizedBox(height: Constants.spacing * 2),
            ],
          ),
        ),
        TextIconButton(
          label: "إنشاء حساب",
          icon: Icons.person_add_alt_1_outlined,
          color: Colors.deepPurple[300],
          labelColor: colorScheme.onPrimary,
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => const RegisterPage(),
                ),
                (r) => false);
          },
        ),
      ],
    );
  }
}
