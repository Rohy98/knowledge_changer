import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/text_form_field_component.dart';
import 'package:knowledge_changer/enums/alert_type.dart';
import 'package:knowledge_changer/models/user_model.dart';
import 'package:knowledge_changer/modules/auth/auth_layout.dart';
import 'package:knowledge_changer/modules/auth/login_page.dart';
import 'package:knowledge_changer/modules/auth/register2_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController password1Controller = TextEditingController();
  final TextEditingController password2Controller = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  bool isLoading = false;
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    return AuthLayout(
      showBackgroundFromTopSide: false,
      boxContent: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            "إنشاء حساب",
            style: TextStyle(fontSize: 25),
          ),
          SizedBox(height: Constants.spacing),
          Form(
            key: formKey,
            child: Column(
              children: [
                TextFormFieldComponent(
                  label: "اسم المستخدم",
                  controller: usernameController,
                  icon: Icons.person_outline,
                  required: true,
                ),
                SizedBox(height: Constants.spacing),
                TextFormFieldComponent(
                  label: "الاسم الأول",
                  controller: firstNameController,
                  icon: Icons.text_format,
                  required: true,
                ),
                SizedBox(height: Constants.spacing),
                TextFormFieldComponent(
                  label: "الاسم الأخير",
                  controller: lastNameController,
                  icon: Icons.text_format,
                  required: true,
                ),
                SizedBox(height: Constants.spacing),
                TextFormFieldComponent(
                  label: "البريد الإلكتروني",
                  controller: emailController,
                  icon: Icons.email_outlined,
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return "هذا الحقل مطلوب";
                    } else if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                      return "يرجى إدخال بريد إلكتروني صالح";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: Constants.spacing),
                TextFormFieldComponent(
                  label: "كلمة المرور",
                  controller: password1Controller,
                  icon: Icons.lock_outline,
                  obscureText: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "هذا الحقل مطلوب";
                    } else if (password1Controller.text != password2Controller.text) {
                      return "لا يوجد تطابق";
                    } else if (!RegExp(r"^(?=.*?[0-9])(?=.*[a-z].*[a-z]).{8,}").hasMatch(value)) {
                      return "يجب أن تكون كلمة المرور لا تقل عن 8 محارف , و يجب أن تحتوي على أرقام و أحرف";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: Constants.spacing),
                TextFormFieldComponent(
                  label: "تأكيد كلمة المرور",
                  controller: password2Controller,
                  icon: Icons.lock_outline,
                  obscureText: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "هذا الحقل مطلوب";
                    } else if (password1Controller.text != password2Controller.text) {
                      return "لا يوجد تطابق";
                    } else {
                      return null;
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
      actions: [
        Visibility(
          visible: isLoading,
          child: Column(
            children: [
              const SizedBox(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              ),
              SizedBox(height: Constants.spacing * 2),
            ],
          ),
        ),
        Visibility(
          visible: !isLoading,
          child: Column(
            children: [
              TextIconButton(
                label: "تسجيل",
                icon: Icons.done,
                onTap: () async {
                  if (formKey.currentState!.validate()) {
                    setState(() {
                      isLoading = true;
                    });
                    var registerRes = await DioHelper.dio.post("/api/register", data: {
                      "username": usernameController.text,
                      "first_name": firstNameController.text,
                      "last_name": lastNameController.text,
                      "password": password1Controller.text,
                      "password2": password2Controller.text,
                      "email": emailController.text,
                    });
                    if (registerRes.statusCode == 201 || registerRes.statusCode == 200) {
                      await SharedUtils.login(usernameController.text, password1Controller.text, AppCubit.get(context));
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const RegisterPage2()), (r) => false);
                      SharedUtils.showSnackBar("تم إنشاء الحساب بنجاح", context, alertType: AlertType.success);
                    } else {
                      if (registerRes.data["username"] != null) {
                        SharedUtils.showSnackBar("يوجد مستخدم موجود بنفس الاسم", context, alertType: AlertType.error);
                      } else if (registerRes.data["email"] != null) {
                        SharedUtils.showSnackBar("البريد الإلكتروني مستخدم مسبقاً", context, alertType: AlertType.error);
                      }
                    }
                    setState(() {
                      isLoading = false;
                    });
                  }
                },
              ),
              SizedBox(height: Constants.spacing * 2),
            ],
          ),
        ),
        TextIconButton(
          label: "لديك حساب بالفعل ؟ تسجيل الدخول",
          icon: Icons.login,
          color: Colors.deepPurple[300],
          labelColor: colorScheme.onPrimary,
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => const LoginPage(),
                ),
                (r) => false);
          },
        )
      ],
    );
  }
}
