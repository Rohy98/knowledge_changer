import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/modules/auth/login_page.dart';
import 'package:knowledge_changer/modules/gallery/gallery_page.dart';
import 'package:knowledge_changer/modules/profile/profile_screen.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedLayout extends StatelessWidget {
  const SharedLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
      listener: (context, states) {},
      builder: (context, states) {
        AppCubit appCubit = AppCubit.get(context);
        return WillPopScope(
          onWillPop: () async => await onBackHandle(context, appCubit),
          child: Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
              appBar: AppBar(
                title: Text(appCubit.activeScreen.title),
                elevation: 0,
                actions: [
                  Visibility(
                    visible: appCubit.activeScreenIndex == 3,
                    child: IconButton(
                      icon: const Icon(Icons.logout),
                      tooltip: "تسجيل الخروج",
                      onPressed: () async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.setString("token", "");
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const LoginPage(),
                          ),
                          (v) => false,
                        );
                        if (appCubit.themeMode == ThemeMode.dark) appCubit.changeThemeMode();
                      },
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      appCubit.themeMode == ThemeMode.dark ? Icons.light_mode_outlined : Icons.dark_mode_outlined,
                    ),
                    tooltip: "الوضع الداكن",
                    onPressed: () => appCubit.changeThemeMode(),
                  ),
                  Visibility(
                    visible: appCubit.user.isAdmin,
                    child: IconButton(
                      icon: const Icon(Icons.photo_library_outlined),
                      tooltip: "إدارة المعرض",
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const GalleryPage(),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: appCubit.activeScreen.screen,
              bottomNavigationBar: BottomNavigationBar(
                selectedItemColor: Theme.of(context).colorScheme.primary,
                elevation: 10,
                type: BottomNavigationBarType.fixed,
                backgroundColor: Theme.of(context).colorScheme.surface,
                currentIndex: appCubit.activeScreenIndex,
                onTap: (int index) {
                  appCubit.changeActiveScreenIndex(index);
                },
                items: Constants.screens
                    .map(
                      (e) => BottomNavigationBarItem(
                        icon: e.screen is ProfileScreen && (appCubit.gender == null || appCubit.country == null || appCubit.birthDate == null)
                            ? Stack(
                                children: [
                                  Icon(e.icon),
                                  const Positioned(
                                    // draw a red marble
                                    top: 0.0,
                                    right: 0.0,
                                    child: Icon(Icons.brightness_1, size: 8.0, color: Colors.redAccent),
                                  )
                                ],
                              )
                            : Icon(e.icon),
                        label: e.title,
                      ),
                    )
                    .toList(),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<bool> onBackHandle(context, AppCubit appCubit) async {
    if (appCubit.activeScreenIndex > 0) {
      appCubit.changeActiveScreenIndex(0);
    } else {
      bool confirmRes = false;
      await showCupertinoModalPopup(
        context: context,
        builder: (_) => ConfirmDialogComponent(
          content: "هل أنت متأكد من إغلاق التطبيق ؟",
          onYesPress: () {
            confirmRes = true;
          },
        ),
      );
      return confirmRes;
    }
    return false;
  }
}
