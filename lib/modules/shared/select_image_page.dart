import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/gallery/gallery_grid_component.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';

class SelectImagePage extends StatelessWidget {
  const SelectImagePage({
    Key? key,
    this.onSelected,
  }) : super(key: key);

  final Function(ImageModel)? onSelected;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
      listener: (context, state) {},
      builder: (context, state) {
        AppCubit appCubit = AppCubit.get(context);
        return Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            appBar: AppBar(
              title: const Text("قم بإختيار صورة"),
            ),
            body: GalleryGridComponent(
              images: appCubit.galleryList,
              onTapImage: (image) {
                if (onSelected != null) {
                  onSelected!(image);
                }
                Navigator.pop(context);
              },
            ),
          ),
        );
      },
    );
  }
}
