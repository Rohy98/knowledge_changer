import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/switch_component.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class SettingsTab extends StatelessWidget {
  const SettingsTab({
    Key? key,
    required this.appCubit,
  }) : super(key: key);

  final AppCubit appCubit;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: Constants.spacing),
            SwitchComponent(
              label: "الوضع النشط",
              value: appCubit.readingMode,
              description: "يستخدم الوضع النشط لمنع إقفال الشاشة أثناء القراءة",
              onChange: (value) {
                appCubit.changeReadingMode();
              },
            ),
            // SizedBox(height: Constants.spacing),
            // SwitchComponent(
            //   label: "تفعيل التنبيهات",
            //   value: appCubit.enableAlerts,
            //   onChange: (value) {
            //     appCubit.changeEnableAlerts();
            //   },
            // ),
            SizedBox(height: Constants.spacing),
            SwitchComponent(
              label: "الوضع المظلم",
              value: appCubit.themeMode == ThemeMode.dark,
              onChange: (value) {
                appCubit.changeThemeMode();
              },
            ),
            // SizedBox(height: Constants.spacing),
            // Row(
            //   children: [
            //     const Text("نمط تقليب الصفحات"),
            //     const SizedBox(width: 20),
            //     Expanded(
            //       child: DropdownButton<PageFlipStyle>(
            //         isExpanded: true,
            //         value: PageFlipStyle.horizontal,
            //         onChanged: (_) {},
            //         items: [
            //           DropdownMenuItem<PageFlipStyle>(
            //             child: Text(PageFlipStyleLocalization.tr(PageFlipStyle.horizontal)),
            //             value: PageFlipStyle.horizontal,
            //           ),
            //           DropdownMenuItem<PageFlipStyle>(
            //             child: Text(PageFlipStyleLocalization.tr(PageFlipStyle.vertical)),
            //             value: PageFlipStyle.vertical,
            //           ),
            //         ],
            //       ),
            //     ),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
