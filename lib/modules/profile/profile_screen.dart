import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:knowledge_changer/components/profile_info_component.dart';
import 'package:knowledge_changer/components/tab_bar.dart';
import 'package:knowledge_changer/modules/profile/account_info_tab.dart';
import 'package:knowledge_changer/modules/profile/contact_us_tap.dart';
import 'package:knowledge_changer/modules/profile/settings_tab.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var colorScheme = Theme.of(context).colorScheme;
    return SingleChildScrollView(
      child: BlocConsumer<AppCubit, AppStates>(
        listener: (context, state) {},
        builder: (context, state) {
          AppCubit appCubit = AppCubit.get(context);
          return Column(
            children: [
              ProfileInfoComponent(
                profileImageLoading: state is SaveImageProfileStartedState,
                user: appCubit.user,
                onDeletePhoto: () {
                  appCubit.changePickedImageProfile(null);
                  appCubit.saveImageProfile();
                },
                onPickedPhoto: (XFile? f) {
                  appCubit.changePickedImageProfile(f);
                  appCubit.saveImageProfile();
                },
              ),
              SizedBox(
                height: Constants.spacing * 2,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: TabBarComponent(
                  children: [
                    TabBarModel(
                      title: "بيانات الحساب",
                      child: AccountInfoTab(appCubit: appCubit),
                    ),
                    TabBarModel(
                      title: "الإعدادات",
                      child: SettingsTab(appCubit: appCubit),
                    ),
                    TabBarModel(
                      title: "اتصل بنا",
                      child: const ContactUsTap(),
                    ),
                    TabBarModel(
                      title: "عن المشروع",
                      child: const Text("عن المشروع"),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
