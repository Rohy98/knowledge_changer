import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/drop_down_search_component.dart';
import 'package:knowledge_changer/enums/alert_type.dart';
import 'package:knowledge_changer/modules/auth/login_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:intl/intl.dart' as intl;
import 'package:knowledge_changer/shared/utils/dio_helper.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountInfoTab extends StatelessWidget {
  const AccountInfoTab({
    Key? key,
    required this.appCubit,
  }) : super(key: key);

  final AppCubit appCubit;

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: SingleChildScrollView(
        child: Column(
          children: [
            TextFormField(
              enabled: false,
              controller: TextEditingController(text: appCubit.user.username),
              decoration: const InputDecoration(
                hintText: "اسم المستخدم",
                icon: Icon(Icons.person_outline),
              ),
            ),
            TextFormField(
              enabled: false,
              controller: TextEditingController(text: appCubit.user.firstName),
              decoration: const InputDecoration(
                hintText: "الاسم الأول",
                icon: Icon(Icons.person_outline),
              ),
            ),
            TextFormField(
              enabled: false,
              controller: TextEditingController(text: appCubit.user.lastName),
              decoration: const InputDecoration(
                hintText: "الاسم الأخير",
                icon: Icon(Icons.person_outline),
              ),
            ),
            TextFormField(
              enabled: false,
              controller: TextEditingController(text: appCubit.user.email),
              decoration: const InputDecoration(
                hintText: "البريد الإلكتروني",
                icon: Icon(Icons.mail_outline_outlined),
              ),
            ),
            InkWell(
              onTap: appCubit.profileInfoEditMode
                  ? () async {
                      final datePick = await showDatePicker(
                        context: context,
                        initialDate: appCubit.birthDate ?? DateTime.now().subtract(const Duration(days: 365 * 5)),
                        firstDate: DateTime(1800),
                        lastDate: DateTime.now().subtract(const Duration(days: 365 * 5)),
                      );
                      if (datePick != null && datePick != appCubit.birthDate) {
                        appCubit.changeBirthDate(datePick);
                      }
                    }
                  : null,
              child: AbsorbPointer(
                child: TextFormField(
                  enabled: appCubit.profileInfoEditMode,
                  controller: TextEditingController(text: appCubit.birthDate != null ? intl.DateFormat("yyyy-MM-dd").format(appCubit.birthDate!) : ""),
                  decoration: const InputDecoration(
                    hintText: "تاريخ الميلاد",
                    icon: Icon(Icons.date_range_outlined),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            DropDownSearchComponent<String>(
              items: Constants.countries,
              label: "البلد",
              popupTitle: "قم باختيار دولتك",
              selectedItem: appCubit.country,
              enabled: appCubit.profileInfoEditMode,
              onChanged: (val) {
                appCubit.changeCountry(val);
              },
            ),
            const SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text("الجنس"),
                Row(
                  children: [
                    Expanded(
                      child: RadioListTile<bool?>(
                        title: const Text("ذكر"),
                        value: appCubit.gender,
                        groupValue: false,
                        onChanged: appCubit.profileInfoEditMode
                            ? (v) {
                                appCubit.changeGender(false);
                              }
                            : null,
                      ),
                    ),
                    Expanded(
                      child: RadioListTile<bool?>(
                        title: const Text("أنثى"),
                        value: appCubit.gender,
                        groupValue: true,
                        onChanged: appCubit.profileInfoEditMode
                            ? (v) {
                                appCubit.changeGender(true);
                              }
                            : null,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: TextIconButton(
                    label: appCubit.profileInfoEditMode ? "إلغاء" : "تعديل",
                    icon: appCubit.profileInfoEditMode ? Icons.close : Icons.edit_outlined,
                    color: appCubit.profileInfoEditMode ? null : colorScheme.primary,
                    labelColor: appCubit.profileInfoEditMode ? null : colorScheme.onPrimary,
                    onTap: appCubit.state is! SaveProfileInfoStartedState
                        ? () {
                            if (appCubit.profileInfoEditMode) {
                              appCubit.cancelProfileInfo();
                            } else {
                              appCubit.changeProfileInfoEditMode(!appCubit.profileInfoEditMode);
                            }
                          }
                        : null,
                  ),
                ),
                Visibility(visible: appCubit.profileInfoEditMode, child: const SizedBox(width: 20)),
                Visibility(
                  visible: appCubit.profileInfoEditMode,
                  child: Expanded(
                    child: Visibility(
                      visible: appCubit.state is! SaveProfileInfoStartedState,
                      child: TextIconButton(
                        label: "حفظ",
                        icon: Icons.done,
                        color: colorScheme.primary,
                        labelColor: colorScheme.onPrimary,
                        onTap: () async {
                          bool res = await appCubit.saveProfileInfo();
                          SharedUtils.showSnackBar(res ? "تم تعديل بياناتك بنجاح" : "يرجى تعبئة الحقول المطلوبة", context, alertType: res ? AlertType.success : AlertType.warning);
                        },
                      ),
                      replacement: const Center(
                        child: SizedBox(
                          width: 50,
                          height: 50,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 30),
            TextIconButton(
              label: "حذف الحساب",
              icon: Icons.delete_outline,
              color: Colors.red,
              onTap: () async {
                var res = await DioHelper.dio.post("/api/delete-account");
                if (res.statusCode == 200) {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.setString("token", "");
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const LoginPage(),
                    ),
                    (v) => false,
                  );
                  if (appCubit.themeMode == ThemeMode.dark) appCubit.changeThemeMode();
                } else {
                  SharedUtils.showSnackBar("حدث خطأ أثناء حذف الحساب", context, alertType: AlertType.error);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
