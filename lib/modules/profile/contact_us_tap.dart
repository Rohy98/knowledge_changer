import 'package:flutter/cupertino.dart';

class ContactUsTap extends StatelessWidget {
  const ContactUsTap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              buildText("البريد الإلكتروني :"),
              buildText("حسابنا على تويتر :"),
            ],
          ),
          const SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildText("Tgh@gmai.com"),
              buildText("@Tgh"),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildText(String text) => SizedBox(
        height: 40,
        child: Center(child: Text(text)),
      );
}
