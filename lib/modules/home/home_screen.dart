import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_sell_sdk_flutter/go_sell_sdk_flutter.dart';
import 'package:knowledge_changer/components/carousel_component.dart';
import 'package:knowledge_changer/components/categories_component.dart';
import 'package:knowledge_changer/models/payment_card.dart';
import 'package:knowledge_changer/shared/utils/payment_handler.dart';
import 'package:knowledge_changer/shared/utils/tap_handler.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                CarouselComponent(),
                CategoriesComponent(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
