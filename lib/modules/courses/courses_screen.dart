import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/courses/course_component.dart';
import 'package:knowledge_changer/components/utils/loading_component.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/modules/courses/course_details_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/courses_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/courses_states.dart';
import 'package:knowledge_changer/shared/bloc/states/favourite_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class CoursesScreen extends StatelessWidget {
  const CoursesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return BlocConsumer<FavouriteCubit, FavouriteStates>(
        listener: (context, state) {},
        builder: (context, state) {
          FavouriteCubit favouriteCubit = FavouriteCubit.get(context);
          return BlocConsumer<CoursesCubit, CoursesStates>(
            listener: (context, state) {},
            builder: (context, state) {
              CoursesCubit coursesCubit = CoursesCubit.get(context);
              return state is! GetCoursesStartedState && state is! GetCoursesCategoriesStartedState
                  ? coursesCubit.coursesList.isNotEmpty
                      ? RefreshIndicator(
                          onRefresh: () async {
                            await coursesCubit.getCourses(false);
                            await coursesCubit.getCoursesCategories(false);
                          },
                          child: ListView.builder(
                            padding: EdgeInsets.all(Constants.spacing),
                            itemCount: coursesCubit.coursesCategoriesList.length,
                            itemBuilder: (context, index) {
                              List<CourseModel> coursesList = coursesCubit.coursesList.where((e) => e.categoryId == coursesCubit.coursesCategoriesList[index].id).toList();
                              return ExpansionTile(
                                title: Text(coursesCubit.coursesCategoriesList[index].name),
                                backgroundColor: Theme.of(context).colorScheme.surface,
                                children: [
                                  ListView.separated(
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    padding: EdgeInsets.symmetric(horizontal: Constants.spacing),
                                    itemCount: coursesList.length,
                                    separatorBuilder: (context, index) => Container(
                                      width: screenSize.width - 50,
                                      height: 1,
                                      color: Colors.grey.withOpacity(0.5),
                                    ),
                                    itemBuilder: (context, index) => CourseComponent(
                                      isFavorite: favouriteCubit.favoriteCoursesList.contains(coursesList[index].id),
                                      course: coursesList[index],
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => CourseDetailsPage(
                                              course: coursesList[index],
                                            ),
                                          ),
                                        );
                                      },
                                      onAddToFavourite: (CourseModel c) async {
                                        await favouriteCubit.addToFavourite("course", c.id);
                                      },
                                      onDeleteFromFavourite: (CourseModel c) async {
                                        await favouriteCubit.deleteFromFavourite("course", c.id);
                                      },
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        )
                      : const IconAndText(
                          text: "لم يتم إضافة دورات حتى الآن",
                          icon: Icons.block,
                        )
                  : const LoadingComponent(text: "جاري تحميل الدورات");
            },
          );
        });
  }
}
