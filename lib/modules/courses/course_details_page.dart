import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:knowledge_changer/components/add_and_show_reviews_component.dart';
import 'package:knowledge_changer/components/line_component.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/land_image_component.dart';
import 'package:knowledge_changer/components/utils/rating_component.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/modules/courses/course_parts_details.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class CourseDetailsPage extends StatelessWidget {
  const CourseDetailsPage({
    Key? key,
    required this.course,
  }) : super(key: key);

  final CourseModel course;

  @override
  Widget build(BuildContext context) {
    final SizedBox heightSpace = SizedBox(
      height: Constants.spacing,
    );

    int lessonsCount = 0;
    for (var i = 0; i < course.partsAndFiles.length; i++) {
      lessonsCount += course.partsAndFiles[i].lessons.length;
    }

    return Stack(
      children: [
        SharedPageComponent(
          appBar: AppBar(
            title: Text(course.name),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                LandImageComponent(image: course.image),
                Padding(
                  padding: EdgeInsets.all(Constants.spacing),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RatingComponent(
                        value: course.rate,
                      ),
                      AddAndShowReviewsComponent(
                        id: course.id,
                        isArticle: false,
                      ),
                      heightSpace,
                      Text(
                        course.name,
                        style: Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.primary),
                        maxLines: 2,
                      ),
                      Html(data: course.intro),
                      const LineComponent(),
                      heightSpace,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "محتوى الدورة",
                            style: Theme.of(context).textTheme.headline6!,
                          ),
                          Visibility(
                            visible: course.partsAndFiles.isNotEmpty || lessonsCount > 0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("عدد الأجزاء  ${course.partsAndFiles.length}"),
                                Text("عدد الدروس  $lessonsCount"),
                              ],
                            ),
                            replacement: const Text("يتم العمل على محتوى هذه الدورة حالياً"),
                          ),
                        ],
                      ),
                      heightSpace,
                      buildStringList(course.skills, "ماذا ستتعلم !؟", context),
                      heightSpace,
                      buildStringList(course.features, "مزايا الدروس", context),
                      Visibility(
                        visible: course.partsAndFiles.isNotEmpty,
                        child: const SizedBox(
                          height: 78,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Visibility(
          visible: course.partsAndFiles.isNotEmpty,
          child: Positioned(
            bottom: Constants.spacing * 2,
            left: Constants.spacing,
            right: Constants.spacing,
            child: TextIconButton(
              label: "هيا نبدأ",
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CoursePartsPage(
                      course: course,
                    ),
                  ),
                );
              },
              color: Theme.of(context).colorScheme.primary,
              labelColor: Theme.of(context).colorScheme.onPrimary,
              icon: Icons.arrow_forward,
            ),
          ),
        ),
      ],
    );
  }

  Widget buildStringList(List<String> strings, String title, context) => Visibility(
        visible: strings.isNotEmpty,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: Theme.of(context).textTheme.headline6!,
            ),
            for (var i = 0; i < strings.length; i++)
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(Icons.done, color: Colors.green[600]),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(child: Text(strings[i])),
                ],
              ),
          ],
        ),
      );
}
