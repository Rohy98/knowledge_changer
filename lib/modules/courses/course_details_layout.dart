import 'package:flutter/material.dart';

class CourseDetailsLayout extends StatelessWidget {
  const CourseDetailsLayout({
    Key? key,
    required this.body,
    this.title,
  }) : super(key: key);

  final Widget body;
  final String? title;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Stack(
      children: [
        Image.asset(
          "assets/images/sky old.png",
          height: screenSize.height,
          width: screenSize.width,
          fit: BoxFit.cover,
        ),
        Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              title: title != null ? Text(title!) : null,
              leading: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white.withOpacity(0.75),
                  child: IconButton(
                    icon: const Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ),
              ),
            ),
            body: body,
          ),
        ),
      ],
    );
  }
}
