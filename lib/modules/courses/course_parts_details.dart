import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/courses/part_component.dart';
import 'package:knowledge_changer/components/line_component.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/modules/courses/course_details_layout.dart';
import 'package:knowledge_changer/modules/courses/part_details_page.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class CoursePartsPage extends StatelessWidget {
  const CoursePartsPage({
    Key? key,
    required this.course,
  }) : super(key: key);

  final CourseModel course;

  @override
  Widget build(BuildContext context) {
    return CourseDetailsLayout(
      title: "أجزاء الدورة ( ${course.name} )",
      body: Padding(
        padding: EdgeInsets.all(Constants.spacing * 2),
        child: ListView.separated(
          itemCount: course.partsAndFiles.length,
          separatorBuilder: (context, index) => Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              SizedBox(
                height: 80,
                width: 1,
                child: LineComponent(
                  color: Colors.white,
                  isHorizantol: false,
                  width: 1,
                ),
              ),
            ],
          ),
          itemBuilder: (context, index) => PartComponent(
            part: course.partsAndFiles[index],
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PartDetailsPage(
                    part: course.partsAndFiles[index],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
