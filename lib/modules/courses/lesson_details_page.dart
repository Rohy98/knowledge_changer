import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/video_player_component.dart';
import 'package:knowledge_changer/models/lesson_model.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

// ignore: must_be_immutable
class LessonDetailsPage extends StatelessWidget {
  LessonDetailsPage({
    Key? key,
    required this.lesson,
  }) : super(key: key);

  final LessonModel lesson;
  bool flag = true;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
    var screenSize = MediaQuery.of(context).size;

    if (lesson.isVideo && flag) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);
      SharedUtils.changeOrientations(Orientation.landscape);
    }

    return !lesson.isVideo
        ? SharedPageComponent(
            appBar: AppBar(
              title: Text(lesson.name),
            ),
            body: SfPdfViewer.network(
              lesson.url,
              key: _pdfViewerKey,
            ),
          )
        : WillPopScope(
            onWillPop: () {
              closeVideo();
              return Future.value(true);
            },
            child: Scaffold(
              body: SizedBox(
                height: screenSize.height,
                width: screenSize.width,
                child: VideoPlayerComponent(
                  url: lesson.url,
                  title: lesson.name,
                  onTapClose: () {
                    closeVideo();
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
          );
  }

  void closeVideo() {
    flag = false;
    SharedUtils.changeOrientations(Orientation.portrait);
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
  }
}
