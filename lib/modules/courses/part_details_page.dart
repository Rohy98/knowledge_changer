import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/courses/lesson_component.dart';
import 'package:knowledge_changer/components/line_component.dart';
import 'package:knowledge_changer/models/part_model.dart';
import 'package:knowledge_changer/modules/courses/course_details_layout.dart';
import 'package:knowledge_changer/modules/courses/lesson_details_page.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class PartDetailsPage extends StatelessWidget {
  const PartDetailsPage({
    Key? key,
    required this.part,
  }) : super(key: key);

  final PartModel part;

  @override
  Widget build(BuildContext context) {
    return CourseDetailsLayout(
      title: "دروس الجزء ( ${part.name} )",
      body: Padding(
        padding: EdgeInsets.all(Constants.spacing * 2),
        child: ListView.separated(
          itemCount: part.lessons.length,
          separatorBuilder: (context, index) => Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              SizedBox(
                height: 80,
                width: 1,
                child: LineComponent(
                  color: Colors.white,
                  isHorizantol: false,
                  width: 1,
                ),
              ),
            ],
          ),
          itemBuilder: (context, index) => LessonComponent(
            lesson: part.lessons[index],
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LessonDetailsPage(
                    lesson: part.lessons[index],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
