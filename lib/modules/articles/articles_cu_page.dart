import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/drop_down_search_component.dart';
import 'package:knowledge_changer/components/utils/text_form_field_component.dart';
import 'package:knowledge_changer/models/category_model.dart';
import 'package:knowledge_changer/modules/shared/select_image_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/categories_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/articles_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class ArticlesCUPage extends StatelessWidget {
  const ArticlesCUPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    var formKey = GlobalKey<FormState>();
    return BlocConsumer<ArticlesCubit, ArticlesStates>(
      listener: (context, state) {},
      builder: (context, state) {
        ArticlesCubit articlesCubit = ArticlesCubit.get(context);
        return SharedPageComponent(
          appBar: AppBar(
            title: const Text("إضافة مقال"),
            actions: [
              AppBarActionComponent(
                loading: state is SaveArticleStartedState,
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    if (articlesCubit.image != null) {
                      articlesCubit.saveArticle().then((value) {
                        if (value) {
                          SharedUtils.showSnackBar("تم حفظ المقال بنجاح", context);
                          Navigator.pop(context);
                        } else {
                          SharedUtils.showSnackBar("حدث خطأ أثناء حفظ المقال", context);
                        }
                      });
                    } else {
                      SharedUtils.showSnackBar("يرجى تحديد صورة من المعرض", context);
                    }
                  }
                },
              ),
            ],
          ),
          body: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.spacing * 2, vertical: Constants.spacing),
                child: Column(
                  children: [
                    TextFormFieldComponent(
                      label: "عنوان المقال",
                      controller: articlesCubit.titleController,
                      icon: Icons.title,
                      required: true,
                    ),
                    SizedBox(height: Constants.spacing),
                    TextFormFieldComponent(
                      label: "كاتب المقال",
                      controller: articlesCubit.writerController,
                      icon: Icons.edit_outlined,
                      required: true,
                    ),
                    SizedBox(height: Constants.spacing),
                    TextFormFieldComponent(
                      label: "المقدمة",
                      controller: articlesCubit.introController,
                      icon: Icons.input_rounded,
                      required: true,
                      keyboardType: TextInputType.multiline,
                    ),
                    SizedBox(height: Constants.spacing),
                    TextFormFieldComponent(
                      label: "المحتوى",
                      controller: articlesCubit.bodyController,
                      icon: Icons.text_snippet_outlined,
                      required: true,
                      keyboardType: TextInputType.multiline,
                    ),
                    SizedBox(height: Constants.spacing * 2),
                    DropDownSearchComponent<CategoryModel>(
                      items: CategoriesCubit.get(context).categoriesList,
                      label: "الصنف",
                      popupTitle: "اختر الصنف الذي يتبع له هذا المقال",
                      selectedItem: articlesCubit.category,
                      onChanged: (CategoryModel? c) {
                        articlesCubit.category = c;
                      },
                    ),
                    SizedBox(height: Constants.spacing * 2),
                    TextIconButton(
                      onTap: () {
                        selectImage(context, articlesCubit);
                      },
                      label: "إختيار صورة من المعرض",
                      icon: Icons.photo_outlined,
                      color: colorScheme.primary,
                      labelColor: colorScheme.onPrimary,
                    ),
                    SizedBox(height: Constants.spacing * 2),
                    InkWell(
                      onTap: () {
                        selectImage(context, articlesCubit);
                      },
                      child: Container(
                        width: 300,
                        height: 300,
                        decoration: BoxDecoration(
                          border: Border.all(width: 1, color: Colors.grey.withOpacity(0.3)),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Visibility(
                          visible: articlesCubit.image != null,
                          child: articlesCubit.image != null
                              ? Image(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(articlesCubit.image!.image),
                                )
                              : const SizedBox(),
                          replacement: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.block, size: 60, color: colorScheme.secondary),
                              const SizedBox(height: 10),
                              const Text("لم يتم تحديد صورة"),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void selectImage(context, articlesCubit) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SelectImagePage(
          onSelected: (image) {
            articlesCubit.changeImage(image);
          },
        ),
      ),
    );
  }
}
