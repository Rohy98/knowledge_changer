import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/articles/articles_grid_component.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/loading_component.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/models/category_model.dart';
import 'package:knowledge_changer/modules/articles/article_details_page.dart';
import 'package:knowledge_changer/modules/articles/articles_cu_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/articles_states.dart';
import 'package:knowledge_changer/shared/bloc/states/favourite_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class ArticlesPage extends StatelessWidget {
  const ArticlesPage({
    Key? key,
    required this.category,
  }) : super(key: key);

  final CategoryModel category;

  @override
  Widget build(BuildContext context) {
    TextEditingController searchController = TextEditingController();
    bool isAdmin = AppCubit.get(context).user.isAdmin;
    return BlocConsumer<FavouriteCubit, FavouriteStates>(
        listener: (context, state) {},
        builder: (context, state) {
          FavouriteCubit favouriteCubit = FavouriteCubit.get(context);
          return BlocConsumer<ArticlesCubit, ArticlesStates>(
            listener: (context, state) {},
            builder: (context, state) {
              ArticlesCubit articlesCubit = ArticlesCubit.get(context);
              return SharedPageComponent(
                appBar: AppBar(
                  title: Text(category.name),
                ),
                body: Stack(
                  children: [
                    articlesCubit.loading
                        ? const LoadingComponent(text: "جاري تحميل المقالات")
                        : Column(
                            children: [
                              Visibility(
                                visible: articlesCubit.selectedCategoryArticlesList.isNotEmpty,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(horizontal: Constants.spacing, vertical: Constants.spacing * 2),
                                      child: TextFormField(
                                        controller: searchController,
                                        decoration: InputDecoration(
                                          suffixIcon: const Icon(Icons.search),
                                          hintText: "البحث عن المقالات",
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(50),
                                          ),
                                          contentPadding: const EdgeInsetsDirectional.only(start: 15),
                                        ),
                                        onChanged: (s) {
                                          articlesCubit.changeSearchText(s);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Visibility(
                                visible: articlesCubit.selectedCategoryArticlesList.isEmpty,
                                child: const Expanded(
                                  child: IconAndText(
                                    text: "لم يتم إضافة أي مقال في هذا التصنيف",
                                    icon: Icons.sms_failed_outlined,
                                  ),
                                ),
                              ),
                              Visibility(
                                visible: articlesCubit.selectedCategoryArticlesList.isNotEmpty,
                                child: Expanded(
                                  child: ArticlesGridComponent(
                                    paddingBottom: isAdmin ? 80 : 0,
                                    onRefresh: () async {
                                      await articlesCubit.getArticles(false);
                                    },
                                    onDeleteFromFavourite: (ArticleModel art) async {
                                      await favouriteCubit.deleteFromFavourite("article", art.id);
                                    },
                                    onAddToFavourite: (ArticleModel art) async {
                                      await favouriteCubit.addToFavourite("article", art.id);
                                    },
                                    favoriteArticlesList: favouriteCubit.favoriteArticlesList,
                                    articlesList: articlesCubit.selectedCategoryArticlesSearchList,
                                    onTapArticle: (ArticleModel a) {
                                      articlesCubit.beginEdit(a);
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => const ArticleDetailsPage(),
                                        ),
                                      );
                                    },
                                    onTapArticleDelete: isAdmin
                                        ? (article) async {
                                            await articlesCubit.delete(article);
                                          }
                                        : null,
                                    emptyDrawer: () => const IconAndText(
                                      text: "لا يوجد عناصر تطابق عملية البحث",
                                      icon: Icons.youtube_searched_for_sharp,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                    Visibility(
                      visible: isAdmin,
                      child: PositionedDirectional(
                        end: Constants.spacing * 2,
                        bottom: Constants.spacing * 2,
                        child: FloatingButtonComponent(
                          onPressed: () {
                            articlesCubit.beginCreate(category);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const ArticlesCUPage(),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        });
  }
}
