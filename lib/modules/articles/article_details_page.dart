import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/add_and_show_reviews_component.dart';
import 'package:knowledge_changer/components/line_component.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/land_image_component.dart';
import 'package:knowledge_changer/components/utils/loading_overlay.dart';
import 'package:knowledge_changer/components/utils/rating_component.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/modules/articles/articles_cu_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/articles_states.dart';
import 'package:knowledge_changer/shared/bloc/states/favourite_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/document_test.dart';
import 'package:intl/intl.dart' as intl;

class ArticleDetailsPage extends StatelessWidget {
  const ArticleDetailsPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final SizedBox heightSpace = SizedBox(
      height: Constants.spacing,
    );
    return BlocConsumer<FavouriteCubit, FavouriteStates>(
      listener: (con, favouriteState) {},
      builder: (con, favouriteState) {
        FavouriteCubit favouriteCubit = FavouriteCubit.get(context);
        return BlocConsumer<ArticlesCubit, ArticlesStates>(
          listener: (con, articlesState) {},
          builder: (con, articlesState) {
            ArticlesCubit articlesCubit = ArticlesCubit.get(context);
            ArticleModel? article = articlesCubit.articleForEdit;
            return SharedPageComponent(
              appBar: AppBar(
                title: Text(article!.title),
                actions: [
                  Visibility(
                    visible: AppCubit.get(context).user.isAdmin,
                    child: AppBarActionComponent(
                      text: "تعديل",
                      icon: Icons.edit_outlined,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const ArticlesCUPage(),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
              body: LoadingOverlay(
                loading: favouriteState is AddHighlightStartedState,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      LandImageComponent(image: article.image),
                      Padding(
                        padding: EdgeInsets.all(Constants.spacing),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RatingComponent(value: article.rate),
                            AddAndShowReviewsComponent(id: article.id),
                            const LineComponent(),
                            heightSpace,
                            Text(
                              article.title,
                              style: Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.primary),
                              maxLines: 2,
                            ),
                            Row(
                              children: [
                                Text(
                                  article.date != null ? intl.DateFormat("yyyy-MM-dd").format(article.date!) : "لا يوجد تاريخ",
                                ),
                                const SizedBox(width: 5),
                                Expanded(
                                  child: Text(
                                    article.writtenBy != null ? article.writtenBy! : "الكاتب غير معروف",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                            heightSpace,
                            Card(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: Constants.spacing, vertical: Constants.spacing * 2),
                                child: RohyHtml(
                                  article: article,
                                  articlesCubit: articlesCubit,
                                  favouriteCubit: favouriteCubit,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
