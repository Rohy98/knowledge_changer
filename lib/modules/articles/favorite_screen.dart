import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/articles/featured_text_list.dart';
import 'package:knowledge_changer/components/favourite/favourite_articles_tab.dart';
import 'package:knowledge_changer/components/favourite/favourite_courses_tab.dart';
import 'package:knowledge_changer/components/tab_bar.dart';
import 'package:knowledge_changer/components/utils/loading_component.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/courses_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/articles_states.dart';
import 'package:knowledge_changer/shared/bloc/states/courses_states.dart';
import 'package:knowledge_changer/shared/bloc/states/favourite_states.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';
import 'package:collection/collection.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CoursesCubit, CoursesStates>(
      listener: (context, state) {},
      builder: (context, coursesState) {
        CoursesCubit coursesCubit = CoursesCubit.get(context);
        return BlocConsumer<ArticlesCubit, ArticlesStates>(
          listener: (context, state) {},
          builder: (context, articlesState) {
            ArticlesCubit articlesCubit = ArticlesCubit.get(context);
            return BlocConsumer<FavouriteCubit, FavouriteStates>(
              listener: (context, state) {},
              builder: (context, favouriteState) {
                FavouriteCubit favouriteCubit = FavouriteCubit.get(context);

                List<FeaturedTextModel> featuredTextList = SharedUtils.fillArticlesInFeaturedTextList(favouriteCubit.featuredTextList, articlesCubit.articlesList);
                List<ArticleModel> favouritArticles = [];
                List<CourseModel> favouritCourses = [];
                if (!articlesCubit.loading && !favouriteCubit.getFavouriteLoading) {
                  for (var i = 0; i < favouriteCubit.favoriteArticlesList.length; i++) {
                    ArticleModel? art = articlesCubit.articlesList.firstWhereOrNull((element) => element.id == favouriteCubit.favoriteArticlesList[i]);
                    if (art != null) favouritArticles.add(art);
                  }
                  for (var i = 0; i < favouriteCubit.favoriteCoursesList.length; i++) {
                    CourseModel? cou = coursesCubit.coursesList.firstWhereOrNull((element) => element.id == favouriteCubit.favoriteCoursesList[i]);
                    if (cou != null) favouritCourses.add(cou);
                  }
                }
                return TabBarComponent(
                  fullHeight: true,
                  underLineStyle: true,
                  bottomSide: true,
                  children: [
                    TabBarModel(
                      title: "المفضلة",
                      child: TabBarComponent(
                        fullHeight: true,
                        underLineStyle: true,
                        children: [
                          TabBarModel(
                            title: "المقالات",
                            child: articlesCubit.loading || favouriteCubit.getFavouriteLoading
                                ? const Center(
                                    child: LoadingComponent(text: "جاري تحميل المقالات المفضلة"),
                                  )
                                : FavouriteArticlesTab(
                                    favouriteCubit: favouriteCubit,
                                    favouritArticles: favouritArticles,
                                  ),
                          ),
                          TabBarModel(
                            title: "الدروس المتقدّمة",
                            child: articlesCubit.loading || favouriteCubit.getFavouriteLoading
                                ? const Center(
                                    child: LoadingComponent(text: "جاري تحميل الدورات المفضلة"),
                                  )
                                : FavouriteCoursesTab(
                                    favouriteCubit: favouriteCubit,
                                    favouritCourses: favouritCourses,
                                  ),
                          ),
                        ],
                      ),
                    ),
                    TabBarModel(
                      title: "علامات مرجعيّة",
                      child: TabBarComponent(
                        fullHeight: true,
                        underLineStyle: true,
                        children: [
                          buildHighlightTap(
                            Icons.star_border_rounded,
                            Colors.amber[500],
                            "لا يوجد نصوص مميزة بنجمة لديك , يمكنك تمييز النصوص التي تريد حفظها في المكتبة عن طريق ضغطة مطوّلة على نص المقال و من ثم النقر على زر النجمة",
                            featuredTextList.where((element) => element.note == "" && !element.isHighlight).toList(),
                            favouriteCubit,
                            articlesCubit,
                          ),
                          buildHighlightTap(
                            Icons.brush_outlined,
                            Colors.pink[600],
                            "لا يوجد نصوص مميزة بلون لديك , يمكنك تمييز النصوص التي تريد حفظها في المكتبة عن طريق ضغطة مطوّلة على نص المقال و من ثم النقر على زر الفرشاة",
                            featuredTextList.where((element) => element.note == "" && element.isHighlight).toList(),
                            favouriteCubit,
                            articlesCubit,
                          ),
                          buildHighlightTap(
                            Icons.comment_bank_outlined,
                            Colors.blue[600],
                            "لم تقم بإضافة أي ملاحظات , يمكنك إضافة ملاحظات على النصوص التي تريدها و حفظها في المكتبة عن طريق ضغطة مطوّلة على نص المقال و من ثم النقر على زر التعليق",
                            featuredTextList.where((element) => element.note != "").toList(),
                            favouriteCubit,
                            articlesCubit,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            );
          },
        );
      },
    );
  }

  TabBarModel buildHighlightTap(IconData icon, Color? iconColor, String emptyMessage, List<FeaturedTextModel> featuredTextList, FavouriteCubit favouriteCubit, ArticlesCubit articlesCubit) => TabBarModel(
        icon: Icon(
          icon,
          color: iconColor,
          size: 30,
        ),
        child: FeaturedTextList(
          emptyIcon: icon,
          emptyIconColor: iconColor,
          emptyMessage: emptyMessage,
          featuredTextList: featuredTextList,
          loading: articlesCubit.loading || favouriteCubit.getHighlightLoading,
          articlesCubit: articlesCubit,
          onTapDelete: (FeaturedTextModel featuredText) async {
            await favouriteCubit.deleteHighlight(featuredText.id!);
          },
        ),
      );
}
// MaterialButton(
//                       onPressed: () {
//                         favouriteCubit.getHighLight();
//                       },
//                       child: Text(
//                         favouriteCubit.favoriteArticlesList.length.toString(),
//                       ),
//                     ),