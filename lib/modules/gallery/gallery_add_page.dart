import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/switch_component.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class GalleryAddPage extends StatelessWidget {
  const GalleryAddPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
      listener: (context, state) {},
      builder: (context, state) {
        AppCubit appCubit = AppCubit.get(context);
        return SharedPageComponent(
          appBar: AppBar(
            title: const Text("إضافة للمعرض"),
            actions: [
              Visibility(
                visible: state is! SaveGalleryObjStartedState,
                child: AppBarActionComponent(
                  onPressed: () {
                    appCubit.saveGalleryObj().then((value) {
                      if (value) {
                        SharedUtils.showSnackBar("تم إضافة الصورة إلى المعرض بنجاح", context);
                        Navigator.pop(context);
                      } else {
                        SharedUtils.showSnackBar("حدث خطأ أثناء رفع الملف", context);
                      }
                    });
                  },
                ),
              ),
              Visibility(
                visible: state is SaveGalleryObjStartedState,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: CircularProgressIndicator(color: appCubit.themeMode == ThemeMode.light ? Colors.white : null),
                ),
              ),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.all(Constants.spacing * 2),
            child: Form(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    TextFormField(
                      controller: appCubit.galleryNameControllerObj,
                      decoration: const InputDecoration(
                        hintText: "الاسم",
                        icon: Icon(Icons.title),
                      ),
                    ),
                    SizedBox(height: Constants.spacing * 2),
                    SwitchComponent(
                      label: "للصور العلوية",
                      value: appCubit.galleryIsCarouselObj,
                      onChange: (v) {
                        appCubit.changeGalleryIsCarouselObj(v);
                      },
                    ),
                    SizedBox(height: Constants.spacing * 2),
                    TextIconButton(
                      onTap: () async {
                        final picker = ImagePicker();
                        var _pickedFile = await picker.pickImage(
                          source: ImageSource.gallery,
                          // imageQuality: 50, // <- Reduce Image quality
                          // maxHeight: 500, // <- reduce the image size
                          // maxWidth: 500
                        );
                        appCubit.changeSelectedImageObj(_pickedFile);
                      },
                      label: "إختيار صورة",
                      icon: Icons.photo_outlined,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
