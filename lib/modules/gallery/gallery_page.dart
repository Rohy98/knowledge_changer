import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/gallery/gallery_grid_component.dart';
import 'package:knowledge_changer/components/shared_page_component.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/modules/gallery/gallery_add_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class GalleryPage extends StatelessWidget {
  const GalleryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SharedPageComponent(
      appBar: AppBar(
        title: const Text("إدارة المعرض"),
      ),
      body: BlocConsumer<AppCubit, AppStates>(
        listener: (context, state) {},
        builder: (context, state) {
          AppCubit appCubit = AppCubit.get(context);
          return Stack(
            children: [
              state is GetGalleryStartedState
                  ? Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          CircularProgressIndicator(),
                          SizedBox(height: 10),
                          Text("جاري تحميل محتوى المعرض"),
                        ],
                      ),
                    )
                  : GalleryGridComponent(images: appCubit.galleryList, onTapImage: (image) {}),
              PositionedDirectional(
                end: Constants.spacing * 2,
                bottom: Constants.spacing * 2,
                child: FloatingButtonComponent(
                  onPressed: () {
                    appCubit.galleryBeginCreateObj();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const GalleryAddPage(),
                      ),
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
