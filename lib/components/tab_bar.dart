import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabBarComponent extends StatelessWidget {
  const TabBarComponent({
    Key? key,
    required this.children,
    this.fullHeight = false,
    this.underLineStyle = false,
    this.bottomSide = false,
  }) : super(key: key);

  final List<TabBarModel> children;
  final bool fullHeight;
  final bool underLineStyle;
  final bool bottomSide;

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    return DefaultTabController(
      length: children.length,
      child: Column(
        verticalDirection: bottomSide ? VerticalDirection.up : VerticalDirection.down,
        children: <Widget>[
          TabBar(
            tabs: children
                .map((e) => Tab(
                      text: e.title,
                      icon: e.icon,
                    ))
                .toList(),
            unselectedLabelColor: Colors.grey,
            labelPadding: EdgeInsets.zero,
            labelColor: underLineStyle ? colorScheme.primary : colorScheme.onPrimary,
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: underLineStyle ? null : BorderRadius.circular(50),
              border: underLineStyle ? Border(bottom: BorderSide(color: colorScheme.primary, width: 2)) : null,
              color: underLineStyle ? null : colorScheme.primary,
            ),
          ),
          getContainer(
            TabBarView(
              children: children.map((tab) => tab.child).toList(),
            ),
            fullHeight,
          )
        ],
      ),
    );
  }

  Widget getContainer(Widget child, bool fullHeight) {
    if (fullHeight) {
      return Expanded(
        child: child,
      );
    } else {
      return SizedBox(
        height: 330,
        child: child,
      );
    }
  }
}

class TabBarModel {
  String? title;
  Icon? icon;
  Widget child;
  TabBarModel({this.title, required this.child, this.icon});
}
