import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/reviews/reviews_bottom_sheet.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/components/utils/rating_component.dart';
import 'package:knowledge_changer/components/utils/text_form_field_component.dart';
import 'package:knowledge_changer/enums/alert_type.dart';
import 'package:knowledge_changer/shared/bloc/cubits/reviews_cubit.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class AddAndShowReviewsComponent extends StatelessWidget {
  const AddAndShowReviewsComponent({
    Key? key,
    required this.id,
    this.isArticle = true,
  }) : super(key: key);

  final int id;
  final bool isArticle;

  @override
  Widget build(BuildContext context) {
    String displayText = isArticle ? "المقال" : "الدورة";
    return Row(
      children: [
        TextIconMaterialButton(
          onTap: () async {
            await showCupertinoModalPopup(
              context: context,
              builder: (_) => NormalDialog(
                doneLoading: true,
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    RatingComponent(
                      readOnly: false,
                      onRatingUpdate: (double value) {
                        ReviewsCubit.get(context).changeReviewRate(value);
                      },
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextFormFieldComponent(
                      label: "أدخل مراجعتك هنا ...",
                      controller: ReviewsCubit.get(context).reviewTextController,
                      icon: Icons.text_format,
                    ),
                  ],
                ),
                onDonePress: () async {
                  if (!await ReviewsCubit.get(context).addReview(id, isArticle)) {
                    SharedUtils.showSnackBar("لقد قمت مسبقاً بإضافة مراجعة على هذا $displayText", context, alertType: AlertType.warning);
                  } else {
                    SharedUtils.showSnackBar("تم إرسال مراجعتك على هذا $displayText , شكراً لاهتمامك", context, alertType: AlertType.success);
                  }
                },
              ),
            );
          },
          label: "تقييم $displayText",
          color: Theme.of(context).colorScheme.secondary,
          icon: Icons.star_half_rounded,
        ),
        TextIconMaterialButton(
          onTap: () {
            if (isArticle) {
              ReviewsCubit.get(context).getArticleReviews(id);
            } else {
              ReviewsCubit.get(context).getCourseReviews(id);
            }
            showModalBottomSheet(
              context: context,
              builder: (context) {
                return ReviewsBottomSheet(
                  id: id,
                  isArticle: isArticle,
                );
              },
            );
          },
          label: "عرض المراجعات",
          color: Colors.blue[500],
          icon: Icons.reviews_outlined,
        ),
      ],
    );
  }
}
