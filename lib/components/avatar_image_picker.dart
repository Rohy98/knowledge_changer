import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class AvatarImagePicker extends StatelessWidget {
  const AvatarImagePicker({
    Key? key,
    this.img,
    this.onTap,
    this.image,
    this.onDeletePhoto,
    this.onPickedPhoto,
    this.loading = false,
    this.onNetwork = true,
  }) : super(key: key);

  final String? img;
  final bool onNetwork;
  final bool loading;
  final File? image;
  final Function()? onTap;
  final Function()? onDeletePhoto;
  final Function(XFile?)? onPickedPhoto;

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    bool isDarkModeEnabeld = Theme.of(context).brightness == Brightness.dark;
    const double picRaduis = 90;
    return Stack(
      alignment: AlignmentDirectional.centerStart,
      clipBehavior: Clip.antiAlias,
      children: [
        InkWell(
          onTap: onTap,
          customBorder: const CircleBorder(),
          child: Container(
            decoration: BoxDecoration(
              color: isDarkModeEnabeld ? const Color(0xff222222) : colorScheme.surface,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(blurRadius: 10, color: Colors.black.withOpacity(0.1), spreadRadius: 5),
              ],
            ),
            child: CircleAvatar(
              backgroundColor: isDarkModeEnabeld ? const Color(0xff222222) : colorScheme.surface,
              radius: picRaduis,
              backgroundImage: img != null && img!.isNotEmpty
                  ? (onNetwork
                      ? NetworkImage(
                          "${DioHelper.baseUrl}/media/${img!}",
                        )
                      : null)
                  : null,
              child: loading
                  ? (const Center(
                      child: CircularProgressIndicator(),
                    ))
                  : (!onNetwork
                      ? (image != null
                          ? Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                              ),
                              clipBehavior: Clip.hardEdge,
                              child: Image.file(image!),
                            )
                          : Icon(
                              Icons.person,
                              size: 130,
                              color: colorScheme.primary,
                            ))
                      : (img != null && img!.isNotEmpty
                          ? null
                          : Icon(
                              Icons.person,
                              size: 130,
                              color: colorScheme.primary,
                            ))),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            if ((img != null && img!.isNotEmpty) || (image != null)) {
              showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Directionality(
                    textDirection: TextDirection.rtl,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                              child: TextIconMaterialButton(
                            label: "اختيار صورة",
                            onTap: () {
                              pickPhoto(context);
                            },
                            icon: Icons.photo_library_outlined,
                            color: Colors.green[600],
                          )),
                          Expanded(
                              child: TextIconMaterialButton(
                            label: "حذف الصورة",
                            onTap: () {
                              showCupertinoModalPopup(
                                context: context,
                                builder: (_) => ConfirmDialogComponent(
                                  content: "هل أنت متأكد من إزالة صورة الملف الشخصي ؟",
                                  onYesPress: () {
                                    if (onDeletePhoto != null) onDeletePhoto!();
                                    Navigator.pop(context);
                                  },
                                ),
                              );
                            },
                            icon: Icons.delete_outline_outlined,
                            color: Colors.red[600],
                          )),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else {
              pickPhoto(null);
            }
          },
          child: Container(
            padding: const EdgeInsetsDirectional.only(top: 5, bottom: 5, start: 5, end: 10),
            decoration: BoxDecoration(color: colorScheme.background, borderRadius: const BorderRadiusDirectional.only(bottomEnd: Radius.circular(50), topEnd: Radius.circular(50)), boxShadow: const [
              BoxShadow(
                blurRadius: 4,
                color: Colors.black26,
                offset: Offset(1, 1),
              )
            ]),
            child: const Icon(Icons.edit_outlined),
          ),
        ),
      ],
    );
  }

  void pickPhoto(BuildContext? context) async {
    final picker = ImagePicker();
    var _pickedFile = await picker.pickImage(
      source: ImageSource.gallery,
    );
    if (_pickedFile != null && onPickedPhoto != null) onPickedPhoto!(_pickedFile);
    if (context != null) Navigator.pop(context);
  }
}
