import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SharedPageComponent extends StatelessWidget {
  const SharedPageComponent({
    Key? key,
    required this.appBar,
    this.body,
    this.bottomSheet,
  }) : super(key: key);

  final PreferredSizeWidget? appBar;
  final Widget? body;
  final Widget? bottomSheet;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: appBar,
        body: body,
        bottomSheet: bottomSheet,
      ),
    );
  }
}
