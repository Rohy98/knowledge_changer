import 'dart:async';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerComponent extends StatefulWidget {
  const VideoPlayerComponent({
    Key? key,
    required this.url,
    this.onTapClose,
    this.title,
  }) : super(key: key);

  final String url;
  final String? title;
  final void Function()? onTapClose;

  @override
  _VideoPlayerComponentState createState() => _VideoPlayerComponentState();
}

class _VideoPlayerComponentState extends State<VideoPlayerComponent> {
  late VideoPlayerController _controller;
  late bool showControls;
  late bool lastTouch;
  late Timer timer;

  @override
  void initState() {
    super.initState();
    showControls = false;
    lastTouch = false;
    _controller = VideoPlayerController.network(widget.url)
      ..initialize().then(
        (_) {
          _controller.play();
          setState(() {});
        },
      );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void toggleShowControls() {
    setState(() {
      showControls = !showControls;
    });
  }

  void checkTimer() {
    if (showControls && !lastTouch) {
      startTimer();
    } else {
      stopTimer();
    }
  }

  void startTimer() {
    lastTouch = true;
    timer = Timer(const Duration(seconds: 3), () {
      toggleShowControls();
      lastTouch = false;
    });
  }

  void stopTimer() {
    timer.cancel();
    lastTouch = false;
  }

  @override
  Widget build(BuildContext context) {
    var viewer = AspectRatio(
      aspectRatio: _controller.value.aspectRatio,
      child: Stack(
        children: [
          VideoPlayer(_controller),
          _ControlsOverlay(
            controller: _controller,
            showControls: showControls,
            title: widget.title,
            onToggle: () {
              setState(() {
                if (_controller.value.isPlaying) {
                  showControls = true;
                  stopTimer();
                } else {
                  startTimer();
                }
                _controller.value.isPlaying ? _controller.pause() : _controller.play();
              });
            },
            onTap: () {
              toggleShowControls();
              checkTimer();
            },
            onTapClose: widget.onTapClose,
          ),
        ],
      ),
    );
    return SizedBox(
      height: 300,
      child: _controller.value.isInitialized
          ? viewer
          : const Center(
              child: SizedBox(
                width: 40,
                height: 40,
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({
    Key? key,
    required this.controller,
    required this.showControls,
    required this.onTap,
    this.onTapClose,
    this.onToggle,
    this.title,
  }) : super(key: key);

  final bool showControls;
  final void Function()? onTap;
  final void Function()? onTapClose;
  final void Function()? onToggle;
  final VideoPlayerController controller;
  final String? title;

  static const _examplePlaybackRates = [0.25, 0.5, 1.0, 1.5, 2.0, 3.0];

  String durationFormatter(Duration d) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(d.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(d.inSeconds.remainder(60));
    return "${twoDigits(d.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  @override
  Widget build(BuildContext context) {
    Duration animationDuration = const Duration(milliseconds: 300);
    Curve animationCurve = Curves.easeInOutExpo;

    AnimatedScale wrapWithAnimatedScale(Widget child, [Alignment alignment = Alignment.center]) => AnimatedScale(
          alignment: alignment,
          curve: animationCurve,
          duration: animationDuration,
          scale: showControls ? 1 : 0,
          child: child,
        );

    return GestureDetector(
      onTap: onTap,
      child: AnimatedContainer(
        width: double.infinity,
        height: double.infinity,
        curve: animationCurve,
        duration: animationDuration,
        color: showControls ? Colors.black.withOpacity(0.85) : Colors.transparent,
        child: Stack(
          children: <Widget>[
            Visibility(
              visible: title != null,
              child: Align(
                alignment: Alignment.topCenter,
                child: wrapWithAnimatedScale(
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Text(
                        title ?? "",
                        style: const TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Alignment.topCenter),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: wrapWithAnimatedScale(
                InkWell(
                  onTap: () {
                    if (onToggle != null) onToggle!();
                  },
                  child: buildIcon(
                    controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                  ),
                ),
              ),
            ),

            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(left: 200.0),
                child: wrapWithAnimatedScale(
                  InkWell(
                    onTap: () {
                      if (onToggle != null) onToggle!();
                      Duration currentPosition = controller.value.position;
                      Duration targetPosition = currentPosition + const Duration(seconds: 10);
                      controller.seekTo(targetPosition);
                    },
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        buildIcon(
                          Icons.redo,
                        ),
                        Positioned(
                          left: 15,
                          bottom: -10,
                          child: buildIcon(Icons.timer_10, 30),
                        )
                      ],
                    ),
                  ),
                  Alignment.centerRight,
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(right: 200.0),
                child: wrapWithAnimatedScale(
                  InkWell(
                    onTap: () {
                      if (onToggle != null) onToggle!();
                      Duration currentPosition = controller.value.position;
                      Duration targetPosition = currentPosition - const Duration(seconds: 10);
                      controller.seekTo(targetPosition);
                    },
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        buildIcon(
                          Icons.undo,
                        ),
                        Positioned(
                          left: 15,
                          bottom: -10,
                          child: buildIcon(Icons.timer_10, 30),
                        )
                      ],
                    ),
                  ),
                  Alignment.centerLeft,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: wrapWithAnimatedScale(
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: InkWell(
                    onTap: onTapClose,
                    child: buildIcon(Icons.close, 30),
                  ),
                ),
                Alignment.topRight,
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: wrapWithAnimatedScale(
                PopupMenuButton<double>(
                  initialValue: controller.value.playbackSpeed,
                  tooltip: 'سرعة الفيديو',
                  onSelected: (speed) {
                    controller.setPlaybackSpeed(speed);
                  },
                  itemBuilder: (context) {
                    return [
                      for (final speed in _examplePlaybackRates)
                        PopupMenuItem(
                          value: speed,
                          child: Text('${speed}x'),
                        )
                    ];
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 16,
                    ),
                    child: Text(
                      '${controller.value.playbackSpeed}x',
                      style: const TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Alignment.topLeft,
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: wrapWithAnimatedScale(
                Padding(
                  padding: const EdgeInsets.only(bottom: 25, right: 10),
                  child: Text(
                    durationFormatter(controller.value.duration),
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                Alignment.bottomRight,
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: wrapWithAnimatedScale(
                ValueListenableBuilder(
                  valueListenable: controller,
                  builder: (context, VideoPlayerValue value, child) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 25, left: 10),
                      child: Text(
                        durationFormatter(value.position),
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  },
                ),
                Alignment.bottomLeft,
              ),
            ),
            AnimatedPositioned(
              duration: animationDuration,
              curve: animationCurve,
              bottom: showControls ? 15 : -15,
              left: 0,
              right: 0,
              child: Directionality(
                textDirection: TextDirection.ltr,
                child: SizedBox(
                  height: 15,
                  child: VideoProgressIndicator(
                    controller,
                    allowScrubbing: true,
                    colors: VideoProgressColors(
                      playedColor: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                ),
              ),
            ),
            // Align(
            //   alignment: Alignment.bottomRight,
            //   child: Padding(
            //     padding: const EdgeInsets.only(bottom: 20),
            //     child: wrapWithAnimatedScale(
            //       IconButton(
            //         onPressed: () {
            //           if (onTapExpand != null) onTapExpand!();
            //         },
            //         icon: Icon(
            //           isExpanded ? Icons.earbuds_sharp : Icons.zoom_out_map_rounded,
            //           color: Colors.white,
            //           size: 40,
            //         ),
            //       ),
            //       Alignment.bottomRight,
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  Widget buildIcon(IconData icon, [double size = 60]) => Icon(
        icon,
        color: Colors.white,
        size: size,
      );
}
