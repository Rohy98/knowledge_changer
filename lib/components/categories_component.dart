import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/category_component.dart';
import 'package:knowledge_changer/components/utils/loading_component.dart';
import 'package:knowledge_changer/models/category_model.dart';
import 'package:knowledge_changer/modules/articles/articles_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/categories_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/categories_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class CategoriesComponent extends StatelessWidget {
  const CategoriesComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CategoriesCubit, CategoriesStates>(
      listener: (context, state) {},
      builder: (context, state) {
        CategoriesCubit categoriesCubit = CategoriesCubit.get(context);
        List<CategoryModel> categoriesList = categoriesCubit.categoriesList;
        if (state is GetCategoriesStartedState) {
          return const SizedBox(
            height: 200,
            child: Center(child: LoadingComponent(text: "جاري تحميل التصنيفات")),
          );
        } else {
          return GridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: EdgeInsets.all(Constants.spacing),
            mainAxisSpacing: Constants.spacing,
            crossAxisSpacing: Constants.spacing,
            children: List.generate(
              categoriesList.length,
              (index) => CategoryComponent(
                onTap: () {
                  ArticlesCubit.get(context).changeSelectedCategoryId(categoriesList[index].id);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ArticlesPage(
                        category: categoriesList[index],
                      ),
                    ),
                  );
                },
                text: categoriesList[index].name,
              ),
            ),
          );
        }
      },
    );
  }
}
