import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/circler_image_component.dart';
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/components/utils/rating_component.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class CourseComponent extends StatefulWidget {
  const CourseComponent({
    Key? key,
    required this.course,
    required this.onTap,
    required this.isFavorite,
    required this.onDeleteFromFavourite,
    required this.onAddToFavourite,
    this.onTapDelete,
  }) : super(key: key);

  final CourseModel course;
  final void Function() onTap;
  final Future<void> Function()? onTapDelete;
  final bool isFavorite;
  final Future<void> Function(CourseModel) onDeleteFromFavourite;
  final Future<void> Function(CourseModel) onAddToFavourite;

  @override
  State<CourseComponent> createState() => _CourseComponentState();
}

class _CourseComponentState extends State<CourseComponent> {
  late bool favouriteLoading;
  Widget widthWhiteSpace = SizedBox(width: Constants.spacing);
  SizedBox heightWhiteSpace = SizedBox(height: Constants.spacing);

  @override
  void initState() {
    favouriteLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Padding(
        padding: EdgeInsets.all(Constants.spacing),
        child: Row(
          children: [
            CirclerImageComponent(image: widget.course.image),
            widthWhiteSpace,
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.course.name,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  heightWhiteSpace,
                  RatingComponent(
                    value: widget.course.rate,
                  ),
                ],
              ),
            ),
            widthWhiteSpace,
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildIconButton(
                  context,
                  favouriteLoading ? null : (widget.isFavorite ? Icons.favorite : Icons.favorite_border_outlined),
                  () async {
                    bool confirmRes = false;
                    if (widget.isFavorite) {
                      await showCupertinoModalPopup(
                        context: context,
                        builder: (_) => ConfirmDialogComponent(
                          content: "هل أنت متأكد من إزالة هذه الدّورة من المفضلة ؟",
                          onYesPress: () {
                            confirmRes = true;
                          },
                        ),
                      );
                      if (confirmRes) {
                        setState(() {
                          favouriteLoading = true;
                        });
                        await widget.onDeleteFromFavourite(widget.course);
                        setState(() {
                          favouriteLoading = false;
                        });
                      }
                    } else {
                      setState(() {
                        favouriteLoading = true;
                      });
                      await widget.onAddToFavourite(widget.course);
                      setState(() {
                        favouriteLoading = false;
                      });
                      SharedUtils.showSnackBar("تم إضافة الدّرس إلى المفضلة", context);
                    }
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildIconButton(context, IconData? icon, Function()? onTap, {Color? iconColor}) {
    return InkWell(
      onTap: onTap,
      child: icon != null
          ? Icon(
              icon,
              color: iconColor ?? Theme.of(context).colorScheme.secondary,
              size: 28,
            )
          : const SizedBox(
              width: 22,
              height: 22,
              child: CircularProgressIndicator(),
            ),
    );
  }
}
