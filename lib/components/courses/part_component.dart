import 'package:flutter/material.dart';
import 'package:knowledge_changer/models/part_model.dart';

class PartComponent extends StatelessWidget {
  const PartComponent({
    Key? key,
    required this.part,
    this.onTap,
  }) : super(key: key);

  final PartModel part;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 110,
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(width: 2, color: Colors.white),
          ),
          child: InkWell(
            onTap: part.lessons.isNotEmpty ? onTap : null,
            child: Center(
              child: Text(
                part.name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 26,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 10,
          top: 10,
          child: CircleAvatar(
            backgroundColor: Colors.white,
            child: Text(
              part.lessons.length.toString(),
              style: const TextStyle(
                color: Colors.black87,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
