// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:knowledge_changer/models/lesson_model.dart';

class LessonComponent extends StatelessWidget {
  const LessonComponent({
    Key? key,
    required this.lesson,
    this.onTap,
  }) : super(key: key);

  final LessonModel lesson;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 180,
      height: 180,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(width: 1, color: Colors.white),
      ),
      child: InkWell(
        customBorder: const CircleBorder(),
        onTap: onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            const SizedBox(height: 5),
            Icon(
              lesson.isVideo ? Icons.video_settings_sharp : Icons.file_copy_outlined,
              size: 40,
              color: Colors.white,
            ),
            Text(
              lesson.name,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 22,
              ),
            ),
            const SizedBox(height: 15),
          ],
        ),
      ),
    );
  }
}
