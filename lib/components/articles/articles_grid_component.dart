import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/articles/article_component.dart';
import 'package:knowledge_changer/components/line_component.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class ArticlesGridComponent extends StatelessWidget {
  const ArticlesGridComponent({
    Key? key,
    required this.articlesList,
    required this.onTapArticle,
    required this.onDeleteFromFavourite,
    required this.onAddToFavourite,
    this.onRefresh,
    this.favoriteArticlesList,
    this.onTapArticleDelete,
    this.emptyDrawer,
    this.paddingBottom = 0,
  }) : super(key: key);

  final List<ArticleModel> articlesList;
  final List<int>? favoriteArticlesList;
  final Widget Function()? emptyDrawer;
  final double paddingBottom;
  final void Function(ArticleModel) onTapArticle;
  final Future<void> Function()? onRefresh;
  final Future<void> Function(ArticleModel) onDeleteFromFavourite;
  final Future<void> Function(ArticleModel) onAddToFavourite;
  final Future<void> Function(ArticleModel)? onTapArticleDelete;

  @override
  Widget build(BuildContext context) {
    return articlesList.isNotEmpty
        ? wrapWithRefreshIndicator(
            ListView.separated(
              itemCount: articlesList.length,
              padding: EdgeInsets.only(bottom: paddingBottom),
              separatorBuilder: (context, index) => const LineComponent(),
              itemBuilder: (context, index) => ArticleComponent(
                isFavorite: favoriteArticlesList != null ? favoriteArticlesList!.contains(articlesList[index].id) : true,
                onAddToFavourite: onAddToFavourite,
                onDeleteFromFavourite: onDeleteFromFavourite,
                article: articlesList[index],
                onTap: () {
                  onTapArticle(articlesList[index]);
                },
                onTapDelete: onTapArticleDelete != null
                    ? () async {
                        await onTapArticleDelete!(articlesList[index]);
                      }
                    : null,
              ),
            ),
          )
        : (emptyDrawer != null
            ? Padding(
                padding: EdgeInsets.all(Constants.spacing * 2),
                child: emptyDrawer!(),
              )
            : const SizedBox());
  }

  Widget wrapWithRefreshIndicator(Widget child) {
    if (onRefresh != null) {
      return RefreshIndicator(child: child, onRefresh: onRefresh!);
    } else {
      return child;
    }
  }
}
