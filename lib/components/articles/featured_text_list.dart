import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/articles/featured_text_component.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';
import 'package:knowledge_changer/modules/articles/article_details_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';

class FeaturedTextList extends StatelessWidget {
  const FeaturedTextList({
    Key? key,
    required this.featuredTextList,
    required this.articlesCubit,
    this.onTap,
    this.onTapDelete,
    this.loading = false,
    required this.emptyMessage,
    required this.emptyIcon,
    this.emptyIconColor,
  }) : super(key: key);

  final List<FeaturedTextModel> featuredTextList;
  final Function(FeaturedTextModel)? onTap;
  final Future<void> Function(FeaturedTextModel)? onTapDelete;
  final bool loading;
  final String emptyMessage;
  final IconData emptyIcon;
  final Color? emptyIconColor;
  final ArticlesCubit articlesCubit;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: !loading
          ? featuredTextList.isNotEmpty
              ? Scrollbar(
                  child: ListView.builder(
                    itemCount: featuredTextList.length,
                    itemBuilder: (context, index) => FeaturedTextComponent(
                      featuredText: featuredTextList[index],
                      onTap: () {
                        articlesCubit.beginEdit(featuredTextList[index].article!);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const ArticleDetailsPage(),
                          ),
                        );
                        if (onTap != null) onTap!(featuredTextList[index]);
                      },
                      onTapDelete: () async {
                        if (onTapDelete != null) await onTapDelete!(featuredTextList[index]);
                      },
                    ),
                  ),
                )
              : IconAndText(
                  text: emptyMessage,
                  icon: emptyIcon,
                  iconColor: emptyIconColor,
                )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
