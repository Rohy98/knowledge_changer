import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/components/utils/outlined_card.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';

class FeaturedTextComponent extends StatefulWidget {
  const FeaturedTextComponent({
    Key? key,
    required this.featuredText,
    this.onTap,
    this.onTapDelete,
  }) : super(key: key);

  final FeaturedTextModel featuredText;
  final Function()? onTap;
  final Future<void> Function()? onTapDelete;

  @override
  State<FeaturedTextComponent> createState() => _FeaturedTextComponentState();
}

class _FeaturedTextComponentState extends State<FeaturedTextComponent> {
  late bool deleteLoading;

  @override
  void initState() {
    deleteLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool noteIsExist = widget.featuredText.note != "";
    var colorScheme = Theme.of(context).colorScheme;
    TextStyle textStyle = TextStyle(color: colorScheme.primary);
    return OutlinedCard(
      elevation: 0.5,
      child: InkWell(
        onTap: widget.onTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(child: Text("من مقال : " + widget.featuredText.article!.title)),
                  const SizedBox(width: 10),
                  Visibility(
                    visible: !deleteLoading,
                    child: IconButton(
                      onPressed: () async {
                        bool confirmRes = false;
                        await showCupertinoModalPopup(
                          context: context,
                          builder: (_) => ConfirmDialogComponent(
                            content: "هل أنت متأكد من إزالة ${noteIsExist ? "ملاحظتك" : "التمييز"} عن هذا النص ؟",
                            onYesPress: () {
                              confirmRes = true;
                            },
                          ),
                        );
                        if (confirmRes && widget.onTapDelete != null) {
                          setState(() {
                            deleteLoading = true;
                          });
                          await widget.onTapDelete!();
                          setState(() {
                            deleteLoading = false;
                          });
                        }
                      },
                      icon: const Icon(Icons.close),
                      color: Colors.grey,
                      iconSize: 30,
                    ),
                  ),
                  Visibility(
                    visible: deleteLoading,
                    child: const SizedBox(
                      width: 22,
                      height: 22,
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              OutlinedCard(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Visibility(
                        visible: noteIsExist,
                        child: Column(
                          children: [
                            Text(
                              "النص",
                              style: textStyle,
                            ),
                            const SizedBox(height: 3),
                          ],
                        ),
                      ),
                      Text(widget.featuredText.highlightenedText),
                    ],
                  ),
                ),
              ),
              SizedBox(height: noteIsExist ? 8 : 0),
              Visibility(
                visible: noteIsExist,
                child: OutlinedCard(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "الملاحظة",
                          style: textStyle,
                        ),
                        const SizedBox(height: 3),
                        Text(widget.featuredText.note),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
