import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:knowledge_changer/components/circler_image_component.dart';
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class ArticleComponent extends StatefulWidget {
  const ArticleComponent({
    Key? key,
    required this.article,
    required this.onTap,
    required this.isFavorite,
    required this.onDeleteFromFavourite,
    required this.onAddToFavourite,
    this.onTapDelete,
  }) : super(key: key);

  final ArticleModel article;
  final void Function() onTap;
  final Future<void> Function()? onTapDelete;
  final bool isFavorite;
  final Future<void> Function(ArticleModel) onDeleteFromFavourite;
  final Future<void> Function(ArticleModel) onAddToFavourite;

  @override
  State<ArticleComponent> createState() => _ArticleComponentState();
}

class _ArticleComponentState extends State<ArticleComponent> {
  late bool deleteLoading;
  late bool favouriteLoading;
  Widget widthWhiteSpace = SizedBox(width: Constants.spacing);
  SizedBox heightWhiteSpace = SizedBox(height: Constants.spacing);

  @override
  void initState() {
    deleteLoading = false;
    favouriteLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Padding(
        padding: EdgeInsets.all(Constants.spacing),
        child: Row(
          children: [
            CirclerImageComponent(image: widget.article.image),
            widthWhiteSpace,
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.article.title,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontSize: 18),
                    maxLines: 1,
                  ),
                  heightWhiteSpace,
                  Row(
                    children: [
                      Expanded(child: Text(widget.article.writtenBy ?? "")),
                      RatingBarIndicator(
                        itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
                        rating: widget.article.rate,
                        itemBuilder: (context, index) => Icon(
                          Icons.star,
                          color: Colors.amber[500],
                        ),
                        itemCount: 5,
                        itemSize: 20,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            widthWhiteSpace,
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildIconButton(
                  context,
                  favouriteLoading ? null : (widget.isFavorite ? Icons.favorite : Icons.favorite_border_outlined),
                  () async {
                    bool confirmRes = false;
                    if (widget.isFavorite) {
                      await showCupertinoModalPopup(
                        context: context,
                        builder: (_) => ConfirmDialogComponent(
                          content: "هل أنت متأكد من إزالة هذا المقال من المكتبة ؟",
                          onYesPress: () {
                            confirmRes = true;
                          },
                        ),
                      );
                      if (confirmRes) {
                        setState(() {
                          favouriteLoading = true;
                        });
                        await widget.onDeleteFromFavourite(widget.article);
                        setState(() {
                          favouriteLoading = false;
                        });
                      }
                    } else {
                      setState(() {
                        favouriteLoading = true;
                      });
                      await widget.onAddToFavourite(widget.article);
                      setState(() {
                        favouriteLoading = false;
                      });
                      SharedUtils.showSnackBar("تم إضافة المقال إلى المكتبة", context);
                    }
                  },
                ),
                const SizedBox(height: 5),
                Visibility(
                  visible: widget.onTapDelete != null,
                  child: buildIconButton(
                    context,
                    !deleteLoading ? Icons.delete_outline_rounded : null,
                    () async {
                      if (widget.onTapDelete != null) {
                        await showCupertinoModalPopup(
                          context: context,
                          builder: (_) => ConfirmDialogComponent(
                            content: "هل أنت متأكد من حذف هذا المقال ؟",
                            onYesPress: () async {
                              setState(() {
                                deleteLoading = true;
                              });
                              await widget.onTapDelete!();
                              setState(() {
                                deleteLoading = false;
                              });
                            },
                          ),
                        );
                      }
                    },
                    iconColor: Colors.red,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Stack(
  //     alignment: AlignmentDirectional.topEnd,
  //     children: [
  //       Card(
  //         elevation: 4,
  //         shadowColor: Colors.black.withOpacity(.2),
  //         clipBehavior: Clip.hardEdge,
  //         shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.circular(10),
  //         ),
  //         child: InkWell(
  //           onTap: widget.onTap,
  //           child: Column(
  //             children: [
  //               Row(),
  //               Expanded(
  //                 child: Image(
  //                   fit: BoxFit.cover,
  //                   image: NetworkImage(widget.imagePath),
  //                   loadingBuilder: SharedUtils.loadingBuilder,
  //                 ),
  //               ),
  //               Padding(
  //                 padding: EdgeInsets.all(Constants.spacing),
  //                 child: Text(widget.text),
  //               ),
  //             ],
  //           ),
  //         ),
  //       ),
  //       buildIconButton(
  //         context,
  //         widget.isFavorite ? Icons.favorite : Icons.favorite_border_outlined,
  //         () {
  //           widget.favoriteOnChange(widget.isFavorite);
  //         },
  //       ),
  //       Visibility(
  //         visible: widget.onTapDelete != null,
  //         child: PositionedDirectional(
  //           start: 3,
  //           child: buildIconButton(
  //             context,
  //             !deleteLoading ? Icons.delete_outline_rounded : null,
  //             () async {
  //               if (widget.onTapDelete != null) {
  //                 await showCupertinoModalPopup(
  //                   context: context,
  //                   builder: (_) => ConfirmDialogComponent(
  //                     content: "هل أنت متأكد من حذف هذا المقال ؟",
  //                     onYesPress: () async {
  //                       setState(() {
  //                         deleteLoading = true;
  //                       });
  //                       await widget.onTapDelete!();
  //                       setState(() {
  //                         deleteLoading = false;
  //                       });
  //                     },
  //                   ),
  //                 );
  //               }
  //             },
  //             iconColor: Colors.red,
  //           ),
  //         ),
  //       ),
  //     ],
  //   );
  // }

  Widget buildIconButton(context, IconData? icon, Function()? onTap, {Color? iconColor}) {
    return InkWell(
      onTap: onTap,
      child: icon != null
          ? Icon(
              icon,
              color: iconColor ?? Theme.of(context).colorScheme.secondary,
              size: 28,
            )
          : const SizedBox(
              width: 22,
              height: 22,
              child: CircularProgressIndicator(),
            ),
    );
  }
}
