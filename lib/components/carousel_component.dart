import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class CarouselComponent extends StatelessWidget {
  const CarouselComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppStates>(
      listener: (context, state) {},
      builder: (context, state) {
        AppCubit appCubit = AppCubit.get(context);
        return CarouselSlider(
          options: CarouselOptions(
            height: 200.0,
            autoPlay: true,
            autoPlayCurve: Curves.easeInOutExpo,
            viewportFraction: 1,
          ),
          items: appCubit.galleryList.where((element) => element.isCarousel).map((i) {
            return Container(
              width: MediaQuery.of(context).size.width,
              // margin: const EdgeInsets.all(5.0),
              color: Theme.of(context).colorScheme.surface,
              child: Image(
                image: NetworkImage(i.image),
                fit: BoxFit.cover,
                loadingBuilder: SharedUtils.loadingBuilder,
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
