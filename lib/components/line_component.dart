import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LineComponent extends StatelessWidget {
  const LineComponent({
    Key? key,
    this.color,
    this.width = 0.5,
    this.isHorizantol = true,
  }) : super(key: key);

  final Color? color;
  final bool isHorizantol;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: isHorizantol ? width : double.infinity,
      width: isHorizantol ? double.infinity : width,
      color: color ?? Theme.of(context).disabledColor.withOpacity(.1),
    );
  }
}
