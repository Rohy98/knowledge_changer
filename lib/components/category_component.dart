import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryComponent extends StatelessWidget {
  const CategoryComponent({
    Key? key,
    required this.text,
    required this.onTap,
    this.color,
  }) : super(key: key);

  final String text;
  final void Function() onTap;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    // ColorScheme colorScheme = Theme.of(context).colorScheme;
    // bool isDarkMode = colorScheme.brightness == Brightness.dark;
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(.3),
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      // color: color ?? (isDarkMode ? colorScheme.surface : colorScheme.primary),
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: Text(
            text,
            // style: TextStyle(color: isDarkMode ? colorScheme.onSurface : colorScheme.onPrimary),
          ),
        ),
      ),
    );
  }
}
