import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class SwitchComponent extends StatelessWidget {
  const SwitchComponent({
    Key? key,
    required this.label,
    required this.value,
    this.description,
    this.onChange,
  }) : super(key: key);

  final String label;
  final bool value;
  final Function(bool)? onChange;
  final String? description;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {
          if (onChange != null) onChange!(!value);
        },
        child: Padding(
          padding: EdgeInsetsDirectional.only(start: Constants.spacing * 2, end: Constants.spacing, bottom: description != null ? Constants.spacing : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(label),
                  const Expanded(child: SizedBox()),
                  Switch(
                    value: value,
                    onChanged: (bool value) {
                      if (onChange != null) onChange!(value);
                    },
                  ),
                ],
              ),
              description != null
                  ? Text(
                      description ?? "",
                      style: TextStyle(color: Theme.of(context).disabledColor),
                    )
                  : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
