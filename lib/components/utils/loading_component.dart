import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingComponent extends StatelessWidget {
  const LoadingComponent({
    Key? key,
    this.text = "يرجى الانتظار",
    this.textColor,
    this.textShadow = false,
  }) : super(key: key);

  final String text;
  final Color? textColor;
  final bool textShadow;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const CircularProgressIndicator(),
          const SizedBox(height: 10),
          Text(
            text,
            style: TextStyle(
              color: textColor,
              shadows: textShadow
                  ? [
                      const Shadow(blurRadius: 2),
                    ]
                  : null,
            ),
          ),
        ],
      ),
    );
  }
}
