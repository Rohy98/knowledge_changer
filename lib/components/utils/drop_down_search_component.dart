import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/models/extendable_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class DropDownSearchComponent<T> extends StatelessWidget {
  const DropDownSearchComponent({
    Key? key,
    required this.items,
    required this.label,
    required this.popupTitle,
    this.searchOnColumnName = "name",
    this.enabled = true,
    this.onChanged,
    this.selectedItem,
  }) : super(key: key);

  final List<T> items;
  final String searchOnColumnName;
  final String label;
  final String popupTitle;
  final Function(T?)? onChanged;
  final T? selectedItem;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return DropdownSearch<T>(
      mode: Mode.DIALOG,
      showSearchBox: true,
      showSelectedItems: true,
      items: items,
      itemAsString: T is String ? (T? t) => (t! as ExtendableModel)[searchOnColumnName].toString() : null,
      // ignore: deprecated_member_use
      label: label,
      selectedItem: selectedItem,
      dropdownSearchDecoration: const InputDecoration(
        contentPadding: EdgeInsets.all(0),
      ),
      searchFieldProps: TextFieldProps(
        textAlign: TextAlign.end,
        decoration: const InputDecoration(
          hintText: "بحث",
        ),
      ),
      popupItemBuilder: (_, t, b) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.spacing * 2, vertical: Constants.spacing),
          child: Text(
            t is String ? t : (t! as ExtendableModel)[searchOnColumnName].toString(),
            textAlign: TextAlign.end,
          ),
        );
      },
      popupTitle: Center(
        child: Column(
          children: [
            const SizedBox(height: 10),
            Text(
              popupTitle,
              style: const TextStyle(
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
      enabled: enabled,
      filterFn: T is String ? (t, filter) => (t! as ExtendableModel)[searchOnColumnName].contains(filter ?? "") : null,
      // popupItemDisabled: (String s) => s.startsWith('I'),
      onChanged: onChanged,
    );
  }
}
