import 'package:flutter/material.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class LandImageComponent extends StatelessWidget {
  const LandImageComponent({
    Key? key,
    required this.image,
  }) : super(key: key);

  final ImageModel? image;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey,
          ),
        ),
      ),
      child: image != null
          ? Image(
              fit: BoxFit.cover,
              image: NetworkImage(image!.image),
              loadingBuilder: SharedUtils.loadingBuilder,
            )
          : const Center(
              child: Text("لم يتم إضافة صورة بعد"),
            ),
    );
  }
}
