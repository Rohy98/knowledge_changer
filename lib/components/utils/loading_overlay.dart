import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:knowledge_changer/components/utils/loading_component.dart';

class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay({
    Key? key,
    required this.child,
    this.loading = false,
  }) : super(key: key);

  final Widget child;
  final bool loading;

  @override
  Widget build(BuildContext context) {
    Widget overlay = Container(
      child: const LoadingComponent(
        textColor: Colors.white,
        textShadow: true,
      ),
      width: double.infinity,
      height: double.infinity,
      color: Colors.black.withOpacity(0.8),
    );
    return Stack(
      children: loading
          ? [
              child,
              overlay,
            ]
          : [child],
    );
  }
}
