import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FloatingButtonComponent extends StatelessWidget {
  const FloatingButtonComponent({
    Key? key,
    required this.onPressed,
    this.icon = Icons.add,
  }) : super(key: key);

  final Function() onPressed;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      // backgroundColor: Theme.of(context).colorScheme.primary,
      onPressed: onPressed,
      child: Icon(icon, color: Theme.of(context).colorScheme.onPrimary),
    );
  }
}

class AppBarActionComponent extends StatelessWidget {
  const AppBarActionComponent({
    Key? key,
    this.text = "حفظ",
    this.icon = Icons.done,
    this.onPressed,
    this.loading = false,
  }) : super(key: key);

  final String text;
  final IconData? icon;
  final Function()? onPressed;
  final bool loading;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircularProgressIndicator(color: Theme.of(context).brightness == Brightness.light ? Colors.white : null),
          )
        : MaterialButton(
            onPressed: onPressed,
            child: Row(
              children: [
                Text(
                  text,
                  style: const TextStyle(color: Colors.white),
                ),
                const SizedBox(
                  width: 5,
                ),
                Icon(
                  icon,
                  color: Colors.white,
                ),
              ],
            ),
          );
  }
}

class TextIconButton extends StatelessWidget {
  const TextIconButton({
    Key? key,
    required this.label,
    required this.onTap,
    this.icon,
    this.color,
    this.labelColor,
    this.width,
  }) : super(key: key);

  final double? width;
  final Color? color;
  final Color? labelColor;
  final String label;
  final IconData? icon;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    var screenSize = MediaQuery.of(context).size;

    return Directionality(
      textDirection: TextDirection.rtl,
      child: MaterialButton(
        onPressed: onTap,
        height: 50,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        minWidth: width ?? screenSize.width,
        color: color ?? colorScheme.surface,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              label,
              style: TextStyle(color: labelColor ?? colorScheme.primary),
            ),
            const SizedBox(width: 10),
            Icon(icon, color: labelColor ?? colorScheme.primary),
          ],
        ),
      ),
    );
  }
}

class TextIconMaterialButton extends StatelessWidget {
  const TextIconMaterialButton({
    Key? key,
    required this.label,
    required this.onTap,
    this.icon,
    this.color,
  }) : super(key: key);

  final Color? color;
  final String label;
  final IconData? icon;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      onPressed: onTap,
      textColor: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(label),
          const SizedBox(width: 2),
          Icon(icon),
        ],
      ),
    );
  }
}
