import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class IconAndText extends StatelessWidget {
  const IconAndText({
    Key? key,
    required this.icon,
    required this.text,
    this.iconColor,
  }) : super(key: key);

  final IconData icon;
  final String text;
  final Color? iconColor;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(),
        Icon(
          icon,
          color: iconColor ?? Theme.of(context).colorScheme.secondary,
          size: 125,
        ),
        SizedBox(
          height: Constants.spacing * 2,
        ),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
