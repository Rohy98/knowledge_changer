import 'package:flutter/material.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class LineComponent extends StatelessWidget {
  const LineComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: Constants.spacing),
      height: 1,
      color: Colors.grey.withOpacity(0.5),
    );
  }
}
