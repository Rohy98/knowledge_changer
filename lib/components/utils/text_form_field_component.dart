import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFormFieldComponent extends StatefulWidget {
  const TextFormFieldComponent({
    Key? key,
    required this.label,
    required this.controller,
    required this.icon,
    this.keyboardType,
    this.obscureText = false,
    this.required = false,
    this.validator,
    this.onChanged,
    this.onTap,
  }) : super(key: key);

  final String label;
  final TextEditingController controller;
  final IconData icon;
  final TextInputType? keyboardType;
  final bool obscureText;
  final bool required;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  final void Function()? onTap;

  @override
  State<TextFormFieldComponent> createState() => _TextFormFieldComponentState();
}

class _TextFormFieldComponentState extends State<TextFormFieldComponent> {
  late bool hidePassword;

  @override
  void initState() {
    hidePassword = widget.obscureText;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: widget.onTap,
      onChanged: widget.onChanged,
      maxLines: widget.keyboardType == TextInputType.multiline ? null : 1,
      obscureText: hidePassword,
      controller: widget.controller,
      keyboardType: widget.keyboardType,
      validator: widget.required
          ? (value) {
              if (value == null || value.trim().isEmpty) {
                return "هذا الحقل مطلوب";
              } else {
                return null;
              }
            }
          : widget.validator,
      decoration: InputDecoration(
        errorMaxLines: 10,
        hintText: widget.label,
        prefixIcon: Icon(widget.icon),
        suffixIcon: widget.obscureText
            ? IconButton(
                onPressed: () {
                  setState(() {
                    hidePassword = !hidePassword;
                  });
                },
                icon: Icon(
                  hidePassword ? Icons.remove_red_eye_rounded : Icons.remove_red_eye_outlined,
                ),
              )
            : null,
      ),
    );
  }
}
