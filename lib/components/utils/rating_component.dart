import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RatingComponent extends StatelessWidget {
  const RatingComponent({
    Key? key,
    this.value,
    this.readOnly = true,
    this.onRatingUpdate,
  }) : super(key: key);

  final double? value;
  final bool readOnly;
  final Function(double)? onRatingUpdate;

  @override
  Widget build(BuildContext context) {
    return readOnly
        ? RatingBarIndicator(
            itemPadding: const EdgeInsets.symmetric(horizontal: 1.0),
            rating: value ?? 0,
            itemBuilder: (context, index) => Icon(
              Icons.star,
              color: Colors.amber[500],
            ),
            itemCount: 5,
            itemSize: 20,
          )
        : RatingBar.builder(
            itemPadding: const EdgeInsets.symmetric(horizontal: 3.0),
            itemBuilder: (context, index) => Icon(
              Icons.star,
              color: Colors.amber[500],
            ),
            onRatingUpdate: onRatingUpdate!,
            itemCount: 5,
            itemSize: 40,
          );
  }
}
