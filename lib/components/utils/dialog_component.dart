import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogComponent extends StatelessWidget {
  const DialogComponent({
    Key? key,
    required this.title,
    required this.content,
    required this.actions,
  }) : super(key: key);

  final String title;
  final Widget content;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: AlertDialog(
        title: Text(
          title,
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
        content: content,
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Wrap(
                children: actions,
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ConfirmDialogComponent extends StatelessWidget {
  const ConfirmDialogComponent({
    Key? key,
    required this.content,
    required this.onYesPress,
    this.title = "تأكيد العملية",
    this.onNoPress,
  }) : super(key: key);

  final String title;
  final String content;
  final Function() onYesPress;
  final Function()? onNoPress;

  @override
  Widget build(BuildContext context) {
    return DialogComponent(
      title: title,
      content: Text(
        content,
        textAlign: TextAlign.center,
      ),
      actions: [
        buildDialogButton(
            text: "نعم",
            icon: Icons.done,
            color: Colors.green[400],
            onPressed: () {
              Navigator.pop(context);
              onYesPress();
            }),
        buildDialogButton(
          text: "لا",
          icon: Icons.close,
          color: Colors.red[400],
          onPressed: () {
            Navigator.pop(context);
            if (onNoPress != null) onNoPress!();
          },
        ),
      ],
    );
  }
}

class EnterTextDialog extends StatelessWidget {
  const EnterTextDialog({
    Key? key,
    required this.onDonePress,
    this.title = "أدخل ملاحظتك هنا",
    this.onCancelPress,
  }) : super(key: key);

  final String title;
  final Function(String) onDonePress;
  final Function()? onCancelPress;

  @override
  Widget build(BuildContext context) {
    TextEditingController c = TextEditingController();
    return NormalDialog(
      title: title,
      content: TextFormField(
        controller: c,
      ),
      onDonePress: () {
        onDonePress(c.text);
      },
      onCancelPress: onCancelPress,
    );
  }
}

class NormalDialog extends StatefulWidget {
  const NormalDialog({
    Key? key,
    required this.onDonePress,
    required this.content,
    this.title = "إضافة مراجعة",
    this.onCancelPress,
    this.doneLoading = false,
  }) : super(key: key);

  final String title;
  final Function() onDonePress;
  final Function()? onCancelPress;
  final bool doneLoading;
  final Widget content;

  @override
  State<NormalDialog> createState() => _NormalDialogState();
}

class _NormalDialogState extends State<NormalDialog> {
  late bool doneLoading;

  @override
  void initState() {
    doneLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DialogComponent(
      title: widget.title,
      content: widget.content,
      actions: [
        buildDialogButton(
            text: "موافق",
            icon: Icons.done,
            color: Colors.green[400],
            loading: doneLoading,
            onPressed: () async {
              if (widget.doneLoading) {
                setState(() {
                  doneLoading = true;
                });
                await widget.onDonePress();
                setState(() {
                  doneLoading = false;
                });
              } else {
                widget.onDonePress();
              }
              Navigator.pop(context);
            }),
        buildDialogButton(
          text: "إلغاء",
          icon: Icons.close,
          color: Colors.red[400],
          onPressed: () {
            Navigator.pop(context);
            if (widget.onCancelPress != null) widget.onCancelPress!();
          },
        ),
      ],
    );
  }
}

Widget buildDialogButton({
  required String text,
  required IconData icon,
  required Function()? onPressed,
  bool loading = false,
  Color? color,
}) =>
    MaterialButton(
      onPressed: loading ? null : onPressed,
      child: Row(
        children: [
          Text(text),
          const SizedBox(width: 5),
          !loading
              ? Icon(
                  icon,
                  color: color ?? Colors.grey,
                )
              : const SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(),
                ),
        ],
      ),
    );
