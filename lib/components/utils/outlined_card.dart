import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OutlinedCard extends StatelessWidget {
  const OutlinedCard({
    Key? key,
    required this.child,
    this.elevation = 0.2,
  }) : super(key: key);

  final Widget? child;
  final double elevation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5), // if you need this
          side: BorderSide(
            color: Colors.grey.withOpacity(elevation),
            width: 1,
          ),
        ),
        child: child,
      ),
    );
  }
}
