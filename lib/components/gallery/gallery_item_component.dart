import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class GalleryItemComponent extends StatelessWidget {
  const GalleryItemComponent({
    Key? key,
    required this.image,
    required this.onTap,
  }) : super(key: key);

  final ImageModel image;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      shadowColor: Colors.black.withOpacity(.2),
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: InkWell(
        onTap: onTap,
        child: Column(
          children: [
            Row(),
            Expanded(
              child: Image(
                fit: BoxFit.cover,
                image: NetworkImage(image.image),
                loadingBuilder: SharedUtils.loadingBuilder,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(Constants.spacing),
              child: Text(image.name),
            ),
          ],
        ),
      ),
    );
  }
}
