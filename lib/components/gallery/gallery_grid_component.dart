import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/gallery/gallery_item_component.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class GalleryGridComponent extends StatelessWidget {
  const GalleryGridComponent({
    Key? key,
    required this.images,
    required this.onTapImage,
  }) : super(key: key);

  final List<ImageModel> images;
  final Function(ImageModel) onTapImage;

  @override
  Widget build(BuildContext context) {
    return images.isNotEmpty
        ? GridView.count(
            crossAxisCount: 2,
            children: List.generate(
              images.length,
              (index) => GalleryItemComponent(
                  image: images[index],
                  onTap: () {
                    onTapImage(images[index]);
                  }),
            ),
          )
        : Padding(
            padding: EdgeInsets.all(Constants.spacing * 2),
            child: const IconAndText(
              text: "المعرض فارغ",
              icon: Icons.block,
            ),
          );
  }
}
