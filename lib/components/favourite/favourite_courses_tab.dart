import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/courses/course_component.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/modules/courses/course_details_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';

class FavouriteCoursesTab extends StatelessWidget {
  const FavouriteCoursesTab({
    Key? key,
    required this.favouriteCubit,
    required this.favouritCourses,
  }) : super(key: key);

  final FavouriteCubit favouriteCubit;
  final List<CourseModel> favouritCourses;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      itemCount: favouritCourses.length,
      separatorBuilder: (context, index) => Container(
        width: double.infinity,
        height: 1,
        color: Colors.grey.withOpacity(0.5),
      ),
      itemBuilder: (context, index) => CourseComponent(
        isFavorite: favouriteCubit.favoriteCoursesList.contains(favouritCourses[index].id),
        course: favouritCourses[index],
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CourseDetailsPage(
                course: favouritCourses[index],
              ),
            ),
          );
        },
        onAddToFavourite: (CourseModel c) async {
          await favouriteCubit.addToFavourite("course", c.id);
        },
        onDeleteFromFavourite: (CourseModel c) async {
          await favouriteCubit.deleteFromFavourite("course", c.id);
        },
      ),
    );
  }
}
