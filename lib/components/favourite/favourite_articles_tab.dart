import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/articles/articles_grid_component.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/modules/articles/article_details_page.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';

class FavouriteArticlesTab extends StatelessWidget {
  const FavouriteArticlesTab({
    Key? key,
    required this.favouriteCubit,
    required this.favouritArticles,
  }) : super(key: key);

  final FavouriteCubit favouriteCubit;
  final List<ArticleModel> favouritArticles;

  @override
  Widget build(BuildContext context) {
    return ArticlesGridComponent(
      onDeleteFromFavourite: (ArticleModel art) async {
        await favouriteCubit.deleteFromFavourite("article", art.id);
      },
      onAddToFavourite: (ArticleModel art) async {
        await favouriteCubit.addToFavourite("article", art.id);
      },
      favoriteArticlesList: favouriteCubit.favoriteArticlesList,
      articlesList: favouritArticles,
      onTapArticle: (a) {
        ArticlesCubit.get(context).beginEdit(a);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const ArticleDetailsPage(),
          ),
        );
      },
      emptyDrawer: () => const IconAndText(
        text: "لا يوجد مقالات في المكتبة, يمكنك وضع قلب للمقال ليتم إضافته هنا مباشرة",
        icon: Icons.favorite_border_outlined,
      ),
    );
  }
}
