import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/components/line_component.dart';
import 'package:knowledge_changer/components/reviews/review_component.dart';
import 'package:knowledge_changer/components/utils/loading_component.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/shared/bloc/cubits/reviews_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/reviews_states.dart';

class ReviewsBottomSheet extends StatelessWidget {
  const ReviewsBottomSheet({
    Key? key,
    required this.id,
    this.isArticle = true,
  }) : super(key: key);

  final int id;
  final bool isArticle;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ReviewsCubit, ReviewsStates>(
      listener: (context, state) {},
      builder: (context, state) {
        ReviewsCubit reviewsCubit = ReviewsCubit.get(context);
        return SizedBox(
          height: double.infinity,
          child: state is! GetReviewsStartedState
              ? Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: (isArticle && reviewsCubit.articleReviewsList.isNotEmpty) || (!isArticle && reviewsCubit.courseReviewsList.isNotEmpty)
                      ? ListView.separated(
                          itemCount: isArticle ? reviewsCubit.articleReviewsList.length : reviewsCubit.courseReviewsList.length,
                          itemBuilder: (context, index) {
                            return ReviewComponent(review: isArticle ? reviewsCubit.articleReviewsList[index] : reviewsCubit.courseReviewsList[index]);
                          },
                          separatorBuilder: (context, builder) => const LineComponent(),
                        )
                      : const IconAndText(icon: Icons.reviews_outlined, text: "لا يوجد مراجعات"),
                )
              : const LoadingComponent(
                  text: "جاري تحميل المراجعات",
                ),
        );
      },
    );
  }
}
