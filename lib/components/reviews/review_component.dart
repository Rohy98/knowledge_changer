import 'package:flutter/material.dart';
import 'package:knowledge_changer/components/utils/rating_component.dart';
import 'package:knowledge_changer/models/review_model.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class ReviewComponent extends StatelessWidget {
  const ReviewComponent({
    Key? key,
    required this.review,
  }) : super(key: key);

  final ReviewModel review;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            review.image != null && review.image!.isNotEmpty
                ? CircleAvatar(
                    radius: 22,
                    backgroundImage: NetworkImage("${DioHelper.baseUrl}/media/${review.image!}"),
                  )
                : const Icon(
                    Icons.person_outline,
                    size: 45,
                  ),
            const SizedBox(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          review.username,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                      RatingComponent(value: review.rate),
                    ],
                  ),
                  Visibility(
                    visible: review.note.isNotEmpty,
                    child: Column(
                      children: [
                        const SizedBox(height: 8),
                        Text(review.note),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
