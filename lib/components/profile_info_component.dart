import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:knowledge_changer/components/avatar_image_picker.dart';
import 'package:knowledge_changer/models/user_model.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';

class ProfileInfoComponent extends StatelessWidget {
  const ProfileInfoComponent({
    Key? key,
    required this.user,
    this.onTap,
    this.onDeletePhoto,
    this.onPickedPhoto,
    this.profileImageLoading = false,
  }) : super(key: key);

  final UserModel user;
  final bool profileImageLoading;
  final Function()? onTap;
  final Function()? onDeletePhoto;
  final Function(XFile?)? onPickedPhoto;

  @override
  Widget build(BuildContext context) {
    var colorScheme = Theme.of(context).colorScheme;
    bool isDarkModeEnabeld = Theme.of(context).brightness == Brightness.dark;
    const double totalAreaHeigth = 200;
    const double coloredAreaHeigth = 120;
    const double picRaduis = 90;
    return Stack(
      children: [
        SizedBox(
          height: totalAreaHeigth,
          width: double.infinity,
          child: Column(
            children: [
              Container(
                height: coloredAreaHeigth,
                width: double.infinity,
                color: isDarkModeEnabeld ? colorScheme.surface : colorScheme.primary,
                padding: EdgeInsetsDirectional.only(end: (picRaduis * 2) + (Constants.spacing * 2), bottom: Constants.spacing, top: Constants.spacing, start: Constants.spacing),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      user.fullName,
                      style: TextStyle(color: isDarkModeEnabeld ? colorScheme.onSurface : colorScheme.onPrimary, fontSize: 16),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              Container(
                height: totalAreaHeigth - coloredAreaHeigth,
                width: double.infinity,
                padding: EdgeInsetsDirectional.only(end: (picRaduis * 2) + (Constants.spacing * 2)),
              ),
            ],
          ),
        ),
        PositionedDirectional(
          end: 20,
          bottom: 0,
          child: AvatarImagePicker(
            loading: profileImageLoading,
            img: user.image,
            onDeletePhoto: onDeletePhoto,
            onPickedPhoto: onPickedPhoto,
            onTap: onTap,
          ),
        ),
      ],
    );
  }
}
