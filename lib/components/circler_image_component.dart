import 'package:flutter/material.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class CirclerImageComponent extends StatelessWidget {
  const CirclerImageComponent({
    Key? key,
    required this.image,
  }) : super(key: key);

  final ImageModel? image;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 60,
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(blurRadius: 5, color: Colors.black26, offset: Offset(1, 1)),
        ],
      ),
      child: image != null
          ? Image(
              fit: BoxFit.cover,
              image: NetworkImage(image!.image),
              loadingBuilder: SharedUtils.loadingBuilder,
            )
          : const Center(child: Text("بلا صورة", style: TextStyle(fontSize: 10))),
    );
  }
}
