class UserModel {
  int id;
  bool isAdmin;
  String username;
  String firstName;
  String lastName;
  String? birthdate;
  bool? gender;
  String? country;
  String? email;
  String? image;
  String get fullName => "$firstName  $lastName";

  UserModel({
    required this.id,
    required this.isAdmin,
    required this.username,
    required this.firstName,
    required this.lastName,
    this.email,
    this.birthdate,
    this.image,
    this.gender,
    this.country,
  });

  factory UserModel.fromJson(dynamic obj) => UserModel(
        id: obj['id'],
        isAdmin: obj['is_admin'] ?? false,
        email: obj['email'],
        username: obj['username'],
        firstName: obj['first_name'],
        lastName: obj['last_name'],
        birthdate: obj['birthdate'],
        gender: obj['gender'] != null ? obj['gender'] == "انثى" : null,
        country: obj['country'],
        image: obj['profileImage'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'is_admin': isAdmin,
        'username': username,
        'first_name': firstName,
        'last_name': lastName,
        'email': email,
        // ignore: prefer_null_aware_operators
        'birthdate': birthdate,
        'gender': gender != null ? (gender! ? "انثى" : "ذكر") : null,
        'country': country,
        'profileImage': image,
      };
}
