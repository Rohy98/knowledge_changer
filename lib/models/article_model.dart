import 'package:knowledge_changer/models/extendable_model.dart';
import 'package:knowledge_changer/models/image_model.dart';

class ArticleModel implements ExtendableModel {
  int id;
  String title;
  String? writtenBy;
  String intro;
  int imageId;
  String body;
  DateTime? date;
  int categoryId;
  ImageModel? image;
  double rate;

  ArticleModel({
    required this.id,
    required this.title,
    required this.intro,
    required this.imageId,
    required this.body,
    required this.categoryId,
    required this.rate,
    this.date,
    this.writtenBy,
  });

  factory ArticleModel.fromJson(dynamic obj) => ArticleModel(
        id: obj['id'],
        title: obj['title'],
        writtenBy: obj['written_by'],
        intro: obj['intro'],
        imageId: obj['image'],
        body: obj['body'],
        date: obj['date'] != null ? DateTime.parse(obj['date']) : null,
        categoryId: obj['category'],
        rate: obj["rate"] ?? 0.0,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'written_by': writtenBy,
        'intro': intro,
        'image': imageId,
        'body': body,
        'date': date.toString(),
        'category': categoryId,
        "rate": rate,
      };

  @override
  dynamic operator [](prop) {
    if (prop == "id") {
      return id;
    } else if (prop == "title") {
      return title;
    } else if (prop == "intro") {
      return intro;
    } else if (prop == "imageId") {
      return imageId;
    } else if (prop == "body") {
      return body;
    } else if (prop == "date") {
      return date;
    } else if (prop == "categoryId") {
      return categoryId;
    }
  }
}
