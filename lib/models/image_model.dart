import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class ImageModel {
  int id;
  String name;
  String image;
  bool isCarousel;

  ImageModel({
    required this.id,
    required this.name,
    required this.image,
    required this.isCarousel,
  });

  factory ImageModel.fromJson(dynamic obj) => ImageModel(
        id: obj['id'],
        name: obj['name'],
        image: "${DioHelper.baseUrl}/media${obj['image']}",
        isCarousel: obj["isCarousel"],
      );
}
