import 'package:knowledge_changer/models/extendable_model.dart';

class CategoryModel implements ExtendableModel {
  int id;
  String name;

  CategoryModel({
    required this.id,
    required this.name,
  });

  @override
  dynamic operator [](prop) {
    if (prop == "id") {
      return id;
    } else {
      return name;
    }
  }

  factory CategoryModel.fromJson(dynamic obj) => CategoryModel(id: obj['id'], name: obj['name']);
}
