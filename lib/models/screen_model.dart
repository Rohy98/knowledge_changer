import 'package:flutter/cupertino.dart';

class ScreenModel {
  final String title;
  final IconData icon;
  final Widget screen;

  ScreenModel({
    required this.screen,
    required this.icon,
    required this.title,
  });
}
