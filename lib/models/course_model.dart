import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/models/part_model.dart';

class CourseModel {
  int id;
  String name;
  String intro;
  List<String> skills;
  List<String> features;
  List<PartModel> partsAndFiles;
  double rate;
  int? categoryId;
  DateTime? date;
  ImageModel? image;
  int? imageId;

  CourseModel({
    required this.id,
    required this.name,
    required this.intro,
    required this.skills,
    required this.features,
    required this.rate,
    required this.partsAndFiles,
    required this.imageId,
    this.categoryId,
    this.date,
  });

  factory CourseModel.fromJson(dynamic obj) {
    // List<LessonModel> ss = [
    //   LessonModel(name: "test test 1", path: "path"),
    //   LessonModel(name: "test test test 2", path: "path"),
    //   LessonModel(name: "test test test 3", path: "path"),
    //   LessonModel(name: "  test 4", path: "path"),
    //   LessonModel(name: "te test 5", path: "path"),
    // ];

    var l = (obj['partsAndFiles'] as List<dynamic>).map((e) => PartModel.fromJson(e)).toList();
    // l.add(PartModel(name: "test 1", lessons: ss));
    // l.add(PartModel(name: "test 2", lessons: ss));
    // l.add(PartModel(name: "test 3", lessons: ss));
    // l.add(PartModel(name: "test 4", lessons: ss));
    // l.add(PartModel(name: "test 5", lessons: ss));
    // l.add(PartModel(name: "test 6", lessons: ss));
    // l.add(PartModel(name: "test 7", lessons: ss));

    return CourseModel(
      id: obj['id'],
      name: obj['courseName'],
      intro: obj['intro'],
      skills: (obj['skills'] as List<dynamic>).map((e) => e as String).toList(),
      features: (obj['features'] as List<dynamic>).map((e) => e as String).toList(),
      partsAndFiles: l,
      imageId: obj['image'],
      date: obj['date'] != null ? DateTime.parse(obj['date']) : null,
      categoryId: obj['category'],
      rate: obj["rate"] ?? 0.0,
    );
  }
}
