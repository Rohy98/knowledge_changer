import 'package:knowledge_changer/models/article_model.dart';

class FeaturedTextModel {
  int? id;
  int articleId;
  ArticleModel? article;
  String highlightenedText;
  String note;
  bool isHighlight;
  String? color;

  FeaturedTextModel({
    this.id,
    required this.articleId,
    required this.highlightenedText,
    this.color,
    this.isHighlight = false,
    required this.note,
    this.article,
  });

  factory FeaturedTextModel.fromJson(dynamic obj) => FeaturedTextModel(
        id: obj['id'],
        articleId: obj['article'],
        highlightenedText: obj['highlightenedText'],
        note: obj['note'],
        isHighlight: obj['isHighlight'],
        color: obj['color'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'article': articleId,
        'highlightenedText': highlightenedText,
        'note': note,
        'isHighlight': isHighlight,
        'color': color ?? "#763209",
      };
}
