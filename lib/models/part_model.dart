import 'package:knowledge_changer/models/lesson_model.dart';

class PartModel {
  String name;
  List<LessonModel> lessons;

  PartModel({required this.name, required this.lessons});

  factory PartModel.fromJson(dynamic obj) {
    return PartModel(
      name: obj["name"],
      lessons: (obj['items'] as List<dynamic>).map((e) => LessonModel.fromJson(e)).toList(),
    );
  }
}
