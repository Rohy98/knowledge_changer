class ReviewModel {
  int? id;
  double rate;
  String note;
  String username;
  String? image;

  ReviewModel({
    this.id,
    required this.rate,
    required this.note,
    required this.username,
    this.image,
  });

  factory ReviewModel.fromJson(dynamic obj) => ReviewModel(
        id: obj['id'],
        rate: obj['rate'],
        note: obj['note'],
        username: obj['user_id__username'],
        image: obj["profileImage"],
      );

  // Map<String, dynamic> toJson() => {
  //       'id': id,
  //       'article': articleId,
  //       'highlightenedText': highlightenedText,
  //       'note': note,
  //       'isHighlight': isHighlight,
  //       'color': color ?? "#763209",
  //     };
}
