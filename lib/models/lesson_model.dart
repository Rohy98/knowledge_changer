import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class LessonModel {
  String name;
  String url;
  bool get isVideo => url.substring(url.length - 3, url.length) != "pdf";

  LessonModel({required this.name, required this.url});

  factory LessonModel.fromJson(dynamic obj) {
    return LessonModel(
      name: obj["fileName"],
      url: "${DioHelper.baseUrl}/media/${obj["file__file"]}",
    );
  }
}
