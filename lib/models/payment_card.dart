class PaymentCard {
  String number;
  String expiryMonth;
  String expiryYear;

  PaymentCard({
    required this.number,
    required this.expiryMonth,
    required this.expiryYear,
  });
}
