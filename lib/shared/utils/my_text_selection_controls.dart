// class MyTextSelectionControls extends MaterialTextSelectionControls {}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

typedef OffsetValue = void Function(int start, int end);

class MyTextSelectionControls extends MaterialTextSelectionControls {
  // Padding between the toolbar and the anchor.
  static const double _kToolbarContentDistanceBelow = 20.0;
  static const double _kToolbarContentDistance = 8.0;
  MyTextSelectionControls({
    required this.buttons,
    this.cancut = false,
    this.cancopy = false,
    this.canpaste = false,
    this.canselectAll = true,
  });

  /// Custom
  final bool cancut;
  final bool cancopy;
  final bool canpaste;
  final bool canselectAll;
  List<TextSelectionToolbarItemData> buttons;

  /// Builder for material-style copy/paste text selection toolbar.
  @override
  Widget buildToolbar(
    BuildContext context,
    Rect globalEditableRegion,
    double textLineHeight,
    Offset selectionMidpoint,
    List<TextSelectionPoint> endpoints,
    TextSelectionDelegate delegate,
    ClipboardStatusNotifier clipboardStatus,
    Offset? lastSecondaryTapDownPosition,
  ) {
    final TextSelectionPoint startTextSelectionPoint = endpoints[0];
    final TextSelectionPoint endTextSelectionPoint = endpoints.length > 1 ? endpoints[1] : endpoints[0];
    final Offset anchorAbove = Offset(globalEditableRegion.left + selectionMidpoint.dx, globalEditableRegion.top + startTextSelectionPoint.point.dy - textLineHeight - _kToolbarContentDistance);
    final Offset anchorBelow = Offset(
      globalEditableRegion.left + selectionMidpoint.dx,
      globalEditableRegion.top + endTextSelectionPoint.point.dy + _kToolbarContentDistanceBelow,
    );

    return MyTextSelectionToolbar(
      anchorAbove: anchorAbove,
      anchorBelow: anchorBelow,
      clipboardStatus: clipboardStatus,
      handleCopy: canCopy(delegate) ? () => handleCopy(delegate, clipboardStatus) : null,
      buttons: buttons,
      delegate: delegate,
      canCopy: cancopy,
      canSelectAll: canselectAll,
      canPaste: canpaste,
      canCut: cancut,
      // customButton: () {
      //   customButton(delegate.textEditingValue.text.substring(delegate.textEditingValue.selection.start, delegate.textEditingValue.selection.end));
      // delegate.userUpdateTextEditingValue(
      //     delegate.textEditingValue.copyWith(
      //       selection: TextSelection.collapsed(
      //         offset: delegate.textEditingValue.selection.baseOffset,
      //       ),
      //     ),
      //     SelectionChangedCause.tap);
      // delegate.textEditingValue = delegate.textEditingValue.copyWith(
      //   selection: TextSelection.collapsed(
      //     offset: delegate.textEditingValue.selection.baseOffset,
      //   ),
      // );
      //   delegate.hideToolbar();
      // },
      handleCut: canCut(delegate) ? () => handleCut(delegate) : null,
      handlePaste: canPaste(delegate) ? () => handlePaste(delegate) : null,
      handleSelectAll: canSelectAll(delegate) ? () => handleSelectAll(delegate) : null,
    );
  }
}

class MyTextSelectionToolbar extends StatefulWidget {
  const MyTextSelectionToolbar({
    Key? key,
    required this.anchorAbove,
    required this.anchorBelow,
    required this.clipboardStatus,
    required this.delegate,
    this.handleCopy,
    this.handleCut,
    this.handlePaste,
    this.handleSelectAll,
    this.canCut = false,
    this.canCopy = false,
    this.canPaste = false,
    this.canSelectAll = true,

    /// Custom
    required this.buttons,
  }) : super(key: key);

  final Offset anchorAbove;
  final Offset anchorBelow;
  final ClipboardStatusNotifier clipboardStatus;
  final VoidCallback? handleCopy;
  final VoidCallback? handleCut;
  final VoidCallback? handlePaste;
  final VoidCallback? handleSelectAll;
  final TextSelectionDelegate delegate;

  final bool canCut;
  final bool canCopy;
  final bool canPaste;
  final bool canSelectAll;
  final List<TextSelectionToolbarItemData> buttons;

  /// Custom

  @override
  MyTextSelectionToolbarState createState() => MyTextSelectionToolbarState();
}

class MyTextSelectionToolbarState extends State<MyTextSelectionToolbar> {
  void _onChangedClipboardStatus() {
    setState(() {
      // Inform the widget that the value of clipboardStatus has changed.
    });
  }

  @override
  void initState() {
    super.initState();
    widget.clipboardStatus.addListener(_onChangedClipboardStatus);
    widget.clipboardStatus.update();
  }

  @override
  void didUpdateWidget(MyTextSelectionToolbar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.clipboardStatus != oldWidget.clipboardStatus) {
      widget.clipboardStatus.addListener(_onChangedClipboardStatus);
      oldWidget.clipboardStatus.removeListener(_onChangedClipboardStatus);
    }
    widget.clipboardStatus.update();
  }

  @override
  void dispose() {
    super.dispose();
    if (!widget.clipboardStatus.disposed) {
      widget.clipboardStatus.removeListener(_onChangedClipboardStatus);
    }
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterialLocalizations(context));
    final MaterialLocalizations localizations = MaterialLocalizations.of(context);

    final List<TextSelectionToolbarItemData> itemDatas = <TextSelectionToolbarItemData>[
      if (widget.handleCut != null && widget.canCut)
        TextSelectionToolbarItemData(
          label: localizations.cutButtonLabel,
          onPressed: (String s) {
            widget.handleCut!();
          },
        ),
      if (widget.handleCopy != null && widget.canCopy)
        TextSelectionToolbarItemData(
          label: localizations.copyButtonLabel,
          onPressed: (String s) {
            widget.handleCopy!();
          },
        ),
      if (widget.handlePaste != null && widget.canPaste && widget.clipboardStatus.value == ClipboardStatus.pasteable)
        TextSelectionToolbarItemData(
          label: localizations.pasteButtonLabel,
          onPressed: (String s) {
            widget.handlePaste!();
          },
        ),
      if (widget.handleSelectAll != null && widget.canSelectAll)
        TextSelectionToolbarItemData(
          label: localizations.selectAllButtonLabel,
          onPressed: (String s) {
            widget.handleSelectAll!();
          },
        ),

      /// Custom
      for (var i = 0; i < widget.buttons.length; i++) widget.buttons[i]
    ];

    int childIndex = 0;
    return TextSelectionToolbar(
      anchorAbove: widget.anchorAbove,
      anchorBelow: widget.anchorBelow,
      toolbarBuilder: (BuildContext context, Widget child) {
        return Card(child: child);
      },
      children: itemDatas.map((TextSelectionToolbarItemData itemData) {
        return TextSelectionToolbarTextButton(
          padding: TextSelectionToolbarTextButton.getPadding(childIndex++, itemDatas.length),
          onPressed: () {
            itemData.onPressed(widget.delegate.textEditingValue.text.substring(widget.delegate.textEditingValue.selection.start, widget.delegate.textEditingValue.selection.end));
          },
          child: itemData.icon != null
              ? Icon(
                  itemData.icon,
                  color: itemData.iconColor,
                )
              : Text(itemData.label!),
        );
      }).toList(),
    );
  }
}

class TextSelectionToolbarItemData {
  const TextSelectionToolbarItemData({
    required this.onPressed,
    this.label,
    this.iconColor,
    this.icon,
  });

  final Function(String) onPressed;
  final String? label;
  final IconData? icon;
  final Color? iconColor;
}
