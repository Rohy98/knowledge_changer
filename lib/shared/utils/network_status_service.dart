import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

enum NetworkStatus { online, offline }

class NetworkStatusService {
  StreamController<NetworkStatus> networkStatusController = StreamController<NetworkStatus>();

  NetworkStatusService() {
    Connectivity().onConnectivityChanged.listen((status) {
      networkStatusController.add(SharedUtils.getNetworkStatus(status));
    });
  }
}
