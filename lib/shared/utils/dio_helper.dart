import 'package:dio/dio.dart';

class DioHelper {
  static late Dio dio;
  static const String baseUrl = "https://shrooqalmehmadi.pythonanywhere.com";

  static init([String token = ""]) {
    dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
        validateStatus: (status) {
          return status != null && status <= 500;
        },
        receiveDataWhenStatusError: true,
        headers: token != ""
            ? {
                "authorization": "Token " + token,
              }
            : null,
      ),
    );
  }
}
