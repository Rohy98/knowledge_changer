import 'package:dio/dio.dart';
import 'package:knowledge_changer/models/payment_card.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class TapHandler {
  static const String _publicKey = "pk_test_lHy6VwfjBMNKOZ1YmGoSX3Id";
  // static const String _secretKey = "sk_test_vx91nKTBJosELVAN5PpODyjg";
  static const String _secretKey = "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ";

  static const String getTokenUrl = "https://api.tap.company/v2/tokens";
  static const String makePaymentUrl = "https://api.tap.company/v2/charges";
  static const headers = {"Authorization": "Bearer $_secretKey"};

  static Future<String> _getToken(PaymentCard card) async {
    var data = {
      "card": {
        "number": card.number,
        "exp_month": card.expiryMonth,
        "exp_year": card.expiryYear,
        "cvc": 100,
      },
    };
    var res = await DioHelper.dio.post(
      getTokenUrl,
      data: data,
      options: Options(
        headers: headers,
      ),
    );
    if (res.statusCode == 200) {
      return res.data['id'];
    }
    return res.data['errors'][0]['description'];
  }

  static Future<void> makePayment(PaymentCard card, int amount) async {
    String token = await _getToken(card);
    var data = {
      "amount": amount,
      "currency": "KWD",
      "source": {"id": "src_kw.knet"},
      "customer": {
        "first_name": "test",
        "middle_name": "test",
        "last_name": "test",
        "email": "test@test.com",
        "phone": {"country_code": "965", "number": "50000000"}
      },
      "redirect": {"url": "http://your_website.com/redirect_url"}
    };
    var res = await DioHelper.dio.post(
      makePaymentUrl,
      data: data,
      options: Options(
        headers: headers,
      ),
    );
    print(token);
    print(res);
  }
}
