import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:knowledge_changer/components/utils/dialog_component.dart';
import 'package:knowledge_changer/enums/alert_type.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';
import 'package:knowledge_changer/shared/utils/my_text_selection_controls.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';

class RohyHtml extends StatelessWidget {
  const RohyHtml({
    Key? key,
    required this.favouriteCubit,
    required this.articlesCubit,
    required this.article,
  }) : super(key: key);

  final FavouriteCubit favouriteCubit;
  final ArticlesCubit articlesCubit;
  final ArticleModel article;

  List<Widget> sipliter(List<dom.Element> elements, String html, context, String type) {
    List<Widget> htmlStackWidgets = [];
    if (elements.isNotEmpty) {
      for (var i = 0; i < elements.length; i++) {
        String imgstr = "";
        if (type == "img") {
          imgstr = elements[i].outerHtml.replaceRange(elements[i].outerHtml.length - 1, elements[i].outerHtml.length, " />");
        }
        List<String> splitResault = html.split(imgstr);
        if (splitResault.length == 2) {
          if (i == 0) {
            htmlStackWidgets.add(_getSelectbleHtml(splitResault[0], context));
          }
          htmlStackWidgets.add(Html(data: imgstr));
          htmlStackWidgets.add(_getSelectbleHtml(splitResault[1], context));
        }
      }
    } else {
      htmlStackWidgets.add(_getSelectbleHtml(html, context));
    }
    return htmlStackWidgets;
  }

  @override
  Widget build(BuildContext context) {
    List<FeaturedTextModel> articleFeaturedTextList = favouriteCubit.featuredTextList.where((element) => element.articleId == article.id).toList();
    articleFeaturedTextList = SharedUtils.fillArticlesInFeaturedTextList(articleFeaturedTextList, articlesCubit.articlesList);
    String renderBody = article.body;
    for (var i = 0; i < articleFeaturedTextList.length; i++) {
      FeaturedTextModel featuredText = articleFeaturedTextList[i];

      List<String> listStr = renderBody.split(featuredText.highlightenedText);
      late String tag;
      if (featuredText.note != "") {
        tag = "ins";
      } else if (featuredText.isHighlight) {
        tag = "bdi";
      } else {
        tag = "mark";
      }
      if (listStr.length == 2) {
        renderBody = '${listStr[0]}<$tag>${featuredText.highlightenedText}</$tag>${listStr[1]}';
      }
    }

    var doc = dom.Document.html(renderBody);
    var imagesTags = doc.getElementsByTagName("img");

    List<Widget> htmlStackWidgets = sipliter(imagesTags, renderBody, context, "img");

    // ol.map((e) => e.replaceWith(dom.Text(e.innerHtml))).toList();
    // ul.map((e) => e.replaceWith(dom.Text(e.innerHtml))).toList();

    return Column(
      children: htmlStackWidgets,
    );
  }

  Widget _getSelectbleHtml(String data, context) {
    late int counter;

    var doc = dom.Document.html(data);
    doc.getElementsByTagName("ol").map((e) => e.replaceWith(dom.Element.html('<div class="ol">${e.innerHtml}</div>'))).toList();
    var ol = doc.getElementsByClassName("ol");
    var ul = doc.getElementsByTagName("ul");
    doc.getElementsByTagName("li").map((e) => e.replaceWith(dom.Element.html("<p>- ${e.innerHtml}</p>"))).toList();
    ol.map((e) {
      counter = 0;
      e.getElementsByTagName("p").map((e) {
        counter++;
        return e.replaceWith(dom.Element.html("<p><b>$counter</b> ${e.innerHtml}</p>"));
      }).toList();
      return e;
    }).toList();

    ul.map((e) => e.replaceWith(dom.Element.html("<div>${e.innerHtml}</div>"))).toList();
    // li.map((e) {
    //   counter++;
    //   e.replaceWith(dom.Element.html("<p>$counter ${e.innerHtml}</p>"));
    // }).toList();
    // ol.map((e) {
    //   e.getElementsByTagName("li").map((e) {
    //     counter++;
    //     e.remove();
    //     htmlStackWidgets.add(
    //       Row(
    //         children: [
    //           Html(
    //             data: "<b>$counter</b>",
    //           ),
    //           _getSelectbleHtml2(dom.Document.html("<p>${e.innerHtml}</p>"), context),
    //         ],
    //       ),
    //     );
    //   }).toList();

    //   e.replaceWith(dom.Element.html("<div>${e.innerHtml}</div>"));
    // }).toList();

    return _getSelectbleHtml2(doc, context);
  }

  Widget _getSelectbleHtml2(dom.Document doc, context) => SelectableHtml.fromDom(
        document: doc,
        selectionControls: MyTextSelectionControls(
          buttons: [
            TextSelectionToolbarItemData(
              icon: Icons.star_border_rounded,
              iconColor: Colors.amber[500],
              onPressed: (String s) {
                addHighlight(
                  context,
                  FeaturedTextModel(
                    articleId: article.id,
                    highlightenedText: s,
                    note: "",
                  ),
                );
              },
            ),
            TextSelectionToolbarItemData(
              icon: Icons.brush_outlined,
              iconColor: Colors.pink[600],
              onPressed: (String s) {
                addHighlight(
                  context,
                  FeaturedTextModel(
                    articleId: article.id,
                    highlightenedText: s,
                    isHighlight: true,
                    note: "",
                  ),
                );
              },
            ),
            TextSelectionToolbarItemData(
              icon: Icons.comment_bank_outlined,
              iconColor: Colors.blue[600],
              onPressed: (String highlitedText) async {
                await showCupertinoModalPopup(
                  context: context,
                  builder: (_) => EnterTextDialog(
                    onDonePress: (String s) {
                      addHighlight(
                        context,
                        FeaturedTextModel(
                          articleId: article.id,
                          highlightenedText: highlitedText,
                          note: s,
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ],
        ),
        // selectionControlsa: FlutterSelectionControls(
        //   toolBarItems: [
        //     ToolBarItem(item: const Text('تحديد الكل'), itemControl: ToolBarItemControl.selectAll),
        //     // ToolBarItem(item: const Text("نسخ"), itemControl: ToolBarItemControl.copy),
        //     // ToolBarItem(item: const Text("قص"), itemControl: ToolBarItemControl.cut),
        //     // ToolBarItem(item: const Text("لصق"), itemControl: ToolBarItemControl.paste),
        //     ToolBarItem(
        //       item: Icon(
        //         Icons.star_border_rounded,
        //         color: Colors.amber[500],
        //       ),
        //       onItemPressed: (String highlightedText, int startIndex, int endIndex) {
        //         addHighlight(
        //           context,
        //           FeaturedTextModel(
        //             articleId: article.id,
        //             highlightenedText: highlightedText,
        //             note: "",
        //           ),
        //         );
        //       },
        //     ),
        //     ToolBarItem(
        //       item: Icon(
        //         Icons.brush_outlined,
        //         color: Colors.pink[600],
        //       ),
        //       onItemPressed: (String highlightedText, int startIndex, int endIndex) {
        //         addHighlight(
        //           context,
        //           FeaturedTextModel(
        //             articleId: article.id,
        //             highlightenedText: highlightedText,
        //             isHighlight: true,
        //             note: "",
        //           ),
        //         );
        //       },
        //     ),
        //     ToolBarItem(
        //       item: Icon(
        //         Icons.comment_bank_outlined,
        //         color: Colors.blue[600],
        //       ),
        //       onItemPressed: (String highlightedText, int startIndex, int endIndex) async {
        //         await showCupertinoModalPopup(
        //           context: context,
        //           builder: (_) => EnterTextDialog(
        //             onDonePress: (String s) {
        //               addHighlight(
        //                 context,
        //                 FeaturedTextModel(
        //                   articleId: article.id,
        //                   highlightenedText: highlightedText,
        //                   note: s,
        //                 ),
        //               );
        //             },
        //           ),
        //         );
        //       },
        //     )
        //   ],
        // ),
        style: {
          "bdi": Style(backgroundColor: Colors.pink[700], color: Colors.white),
          "ins": Style(backgroundColor: Colors.blue[700], color: Colors.white),
        },
      );

  void addHighlight(context, FeaturedTextModel f) {
    favouriteCubit
        .addHighlight(
          f,
        )
        .then((value) => SharedUtils.showSnackBar("تمت حفظه في المكتبة", context, alertType: AlertType.success));
  }
}
