import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:knowledge_changer/enums/alert_type.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';
import 'package:collection/collection.dart';
import 'package:knowledge_changer/models/user_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';
import 'package:knowledge_changer/shared/utils/network_status_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class SharedUtils {
  static Widget loadingBuilder(con, child, loadingProgress) {
    if (loadingProgress == null) return child;
    return Center(
      child: CircularProgressIndicator(
        value: loadingProgress.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes! : null,
      ),
    );
  }

  static void showSnackBar(String text, context, {AlertType? alertType}) => ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Row(
            children: [
              Expanded(
                child: Text(
                  text,
                  style: const TextStyle(
                    fontFamily: "Noto",
                  ),
                ),
              ),
              Visibility(
                visible: alertType != null,
                child: Icon(
                  alertType == AlertType.success
                      ? Icons.done
                      : alertType == AlertType.error
                          ? Icons.close
                          : Icons.warning_amber_rounded,
                  color: alertType == AlertType.success
                      ? Colors.green[600]
                      : alertType == AlertType.warning
                          ? Colors.amber[600]
                          : Colors.red[600],
                ),
              ),
            ],
          ),
        ),
      );

  static List<FeaturedTextModel> fillArticlesInFeaturedTextList(List<FeaturedTextModel> featuredTextList, List<ArticleModel> articles) {
    List<FeaturedTextModel> f = [];
    for (var i = 0; i < featuredTextList.length; i++) {
      FeaturedTextModel el = featuredTextList[i];
      ArticleModel? art = articles.firstWhereOrNull((element) => element.id == el.articleId);
      if (art != null) {
        f.add(el..article = art);
      }
    }
    return f;
  }

  static changeOrientations(Orientation o) {
    if (o == Orientation.landscape) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
    } else {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    }
  }

  static String dateFormatter(DateTime date, [String seprator = '/']) => "${date.year}$seprator${date.month}$seprator${date.day}";

  static NetworkStatus getNetworkStatus(ConnectivityResult status) {
    return status == ConnectivityResult.mobile || status == ConnectivityResult.wifi ? NetworkStatus.online : NetworkStatus.offline;
  }

  static Future<bool> login(String username, String password, AppCubit appCubit) async {
    var loginRes = await DioHelper.dio.post("/api/token", data: {
      "username": username,
      "password": password,
    });
    if (loginRes.statusCode == 200) {
      String token = loginRes.data["token"].toString();
      if (loginRes.data["token"].toString().isNotEmpty) {
        DioHelper.init(token);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        UserModel user = UserModel.fromJson(loginRes.data);
        prefs.setString("token", token);
        prefs.setString("user", jsonEncode(user.toJson()));
        appCubit.changeUser(user);
        appCubit.initApp();
        return true;
      }
      return false;
    } else {
      return false;
    }
  }
}
