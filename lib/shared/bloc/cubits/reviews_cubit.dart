import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/models/review_model.dart';
import 'package:knowledge_changer/shared/bloc/states/reviews_states.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class ReviewsCubit extends Cubit<ReviewsStates> {
  ReviewsCubit() : super(InitReviewsState());

  late List<ReviewModel> articleReviewsList;
  late List<ReviewModel> courseReviewsList;

  late TextEditingController reviewTextController;
  late double reviewRate;

  static ReviewsCubit get(BuildContext context) => BlocProvider.of(context);

  void initReviewsCubit() async {
    articleReviewsList = [];
    courseReviewsList = [];
    reviewTextController = TextEditingController();
    reviewRate = 0;
    emit(InitReviewsState());
  }

  void changeReviewRate(double value) {
    reviewRate = value;
  }

  Future<void> getArticleReviews(int articleId, [bool withLoading = true]) async {
    articleReviewsList = [];
    if (withLoading) {
      emit(GetReviewsStartedState());
    }
    var res = await DioHelper.dio.get('/api/get_article_reviews/$articleId');
    for (var item in res.data) {
      articleReviewsList.add(ReviewModel.fromJson(item));
    }
    emit(GetReviewsFinishedState());
  }

  Future<void> getCourseReviews(int courseId, [bool withLoading = true]) async {
    courseReviewsList = [];
    if (withLoading) {
      emit(GetReviewsStartedState());
    }
    var res = await DioHelper.dio.get('/api/get_course_reviews/$courseId');
    for (var item in res.data) {
      courseReviewsList.add(ReviewModel.fromJson(item));
    }
    emit(GetReviewsFinishedState());
  }

  Future<bool> addReview(int id, [bool isArticle = true]) async {
    String type = isArticle ? "article" : "course";
    var res = await DioHelper.dio.post("/api/add_${type}_review", data: {
      "rate": reviewRate,
      "note": reviewTextController.text,
      "${type}_id": id,
    });
    emit(AddReviewState());
    if (res.data == "Already Exist!") {
      return false;
    }
    reviewTextController.clear();
    return true;
  }
}
