import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/enums/cubit_mode.dart';
import 'package:knowledge_changer/models/article_model.dart';
import 'package:knowledge_changer/models/category_model.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/categories_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/articles_states.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class ArticlesCubit extends Cubit<ArticlesStates> {
  ArticlesCubit(this.appCubit, this.categoriesCubit) : super(InitArticlesCubitState());

  final AppCubit appCubit;
  final CategoriesCubit categoriesCubit;

  // List Start

  List<ArticleModel> articlesList = [];
  // List<FeaturedTextModel> featuredTextList = [];
  List<ArticleModel> selectedCategoryArticlesList = [];
  List<ArticleModel> selectedCategoryArticlesSearchList = [];
  int? selectedCategoryId;
  String? searchText;
  bool loading = false;

  static ArticlesCubit get(BuildContext context) => BlocProvider.of(context);

  void initArticlesCubit() async {
    await getArticles();
    // getFeaturedTextFromPrefs();
    emit(InitArticlesCubitState());
  }

  void changeLoading(bool value) {
    loading = value;
    emit(ChangeLoadingState());
  }

  Future<void> getArticles([bool withLoading = true]) async {
    if (withLoading) {
      emit(GetArticlesStartedState());
      changeLoading(true);
    }
    var res = await DioHelper.dio.get('/api/articles');
    articlesList = [];
    for (var item in res.data) {
      var a = ArticleModel.fromJson(item);
      a.image = appCubit.galleryList.firstWhere((element) => element.id == a.imageId);
      articlesList.add(a);
    }
    changeSelectedCategoryArticlesList();
    search();
    if (withLoading) {
      changeLoading(false);
    }
    emit(GetArticlesFinishedState());
  }

  // void deleteFromFeaturedTextList(FeaturedTextModel m) {
  //   featuredTextList.remove(m);
  //   saveFeaturedTextInPrefs();
  //   emit(DeleteFromFeaturedTextListState());
  // }

  // void addFromFeaturedTextList(FeaturedTextModel m) {
  //   featuredTextList.add(m);
  //   saveFeaturedTextInPrefs();
  //   emit(AddToFeaturedTextListState());
  // }

  // void saveFeaturedTextInPrefs() {
  //   prefs.setString("featuredText", convertListToString(featuredTextList));
  // }

  // void getFeaturedTextFromPrefs() {
  // String? s = prefs.getString("featuredText");
  // if (s != null) {
  //   List<FeaturedTextModel> list = [];
  //   List<dynamic> mapList = jsonDecode(s);
  //   for (var i = 0; i < mapList.length; i++) {
  //     var el = mapList[i];
  //     var article = articlesList.firstWhereOrNull((element) => element.id == el["articleId"]);
  //     if (article != null) {
  //       list.add(
  //         FeaturedTextModel(article: article, text: el["text"], note: el['note'], heighlight: el["heighlight"]),
  //       );
  //     }
  //   }
  //   emit(GetFeaturedTextListState());
  //   featuredTextList = list;
  // }
  // }

  String convertListToString(List<FeaturedTextModel> list) {
    String str = "[";
    for (var i = 0; i < list.length; i++) {
      var obj = jsonEncode(list[i].toJson());
      str += obj;
      if (i == list.length - 1) {
        str += "]";
      } else {
        str += ",";
      }
    }
    return str;
  }

  void changeSelectedCategoryId(int categoryId) async {
    selectedCategoryId = categoryId;
    changeSelectedCategoryArticlesList();
    changeSearchText("");
    emit(ChangeSelectedCategoryArticlesListState());
  }

  void changeSelectedCategoryArticlesList() async {
    if (selectedCategoryId != null) {
      selectedCategoryArticlesList = articlesList.where((e) => e.categoryId == selectedCategoryId).toList();
    } else {
      selectedCategoryArticlesList = [];
    }
    selectedCategoryArticlesSearchList = selectedCategoryArticlesList;
    emit(ChangeSelectedCategoryArticlesListState());
  }

  void changeSearchText(String? s) {
    searchText = s;
    search();
  }

  void search() {
    if (searchText != null && searchText!.isNotEmpty) {
      selectedCategoryArticlesSearchList = selectedCategoryArticlesList.where((element) => element.title.contains(searchText!)).toList();
    } else {
      selectedCategoryArticlesSearchList = selectedCategoryArticlesList;
    }
    emit(ChangeSearchTextState());
  }

  // List End

  // Add And Edit Start

  TextEditingController titleController = TextEditingController();
  TextEditingController writerController = TextEditingController();
  TextEditingController introController = TextEditingController();
  TextEditingController bodyController = TextEditingController();
  DateTime? date;
  ImageModel? image;
  CategoryModel? category;
  CubitMode cubitMode = CubitMode.create;
  ArticleModel? articleForEdit;

  void beginCreate(CategoryModel? c) {
    titleController.clear();
    introController.clear();
    bodyController.clear();
    writerController.clear();
    image = null;
    date = DateTime.now();
    category = c;
    cubitMode = CubitMode.create;
  }

  void beginEdit(ArticleModel a) {
    articleForEdit = a;
    titleController.text = a.title;
    introController.text = a.intro;
    bodyController.text = a.body;
    writerController.text = a.writtenBy ?? "";
    image = a.image;
    date = a.date;
    category = categoriesCubit.categoriesList.firstWhereOrNull((element) => element.id == a.categoryId);
    cubitMode = CubitMode.edit;
  }

  Future<bool> saveArticle() async {
    emit(SaveArticleStartedState());
    bool result = false;
    if (category != null) {
      var formData = FormData.fromMap({
        "title": titleController.text,
        "written_by": writerController.text,
        "intro": introController.text.replaceAll('\n', '<br>'),
        "image": image!.id,
        "body": bodyController.text.replaceAll('\n', '<br>'),
        "date": date,
        "category": category!.id,
      });
      var res = await DioHelper.dio.post(cubitMode == CubitMode.create ? '/api/create_article' : '/api/update_article/${articleForEdit!.id}/', data: formData);
      if (res.statusCode == 200) {
        ArticleModel obj = ArticleModel.fromJson(res.data);
        obj.image = appCubit.galleryList.firstWhere((element) => element.id == obj.imageId);
        if (cubitMode == CubitMode.create) {
          articlesList.add(obj);
        } else {
          ArticleModel? a = articlesList.firstWhereOrNull((element) => element.id == obj.id);
          if (a != null) {
            a.title = obj.title;
            a.body = obj.body;
            a.intro = obj.intro;
            a.categoryId = obj.categoryId;
            a.date = obj.date;
            a.imageId = obj.imageId;
            a.image = obj.image;
            a.writtenBy = obj.writtenBy;
          }
        }
        changeSelectedCategoryArticlesList();
        result = true;
      }
    }
    emit(SaveArticleFinishedState());
    return result;
  }

  void changeCategory(CategoryModel? c) {
    category = c;
    emit(ChangeCategoryState());
  }

  void changeImage(ImageModel? i) {
    image = i;
    emit(ChangeImageState());
  }

  // Add And Edit End

  // Start Delete

  Future<void> delete(ArticleModel article) async {
    var res = await DioHelper.dio.post('/api/delete_article/${article.id}/');
    if (res.statusCode == 200) {
      articlesList.remove(article);
      changeSelectedCategoryArticlesList();
    }
    emit(DeleteArticleState());
  }

  // End Delete
}
