import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:knowledge_changer/enums/cubit_mode.dart';
import 'package:knowledge_changer/models/image_model.dart';
import 'package:knowledge_changer/models/screen_model.dart';
import 'package:knowledge_changer/models/user_model.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/constants/constants.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wakelock/wakelock.dart';
import 'package:intl/intl.dart' as intl;

class AppCubit extends Cubit<AppStates> {
  AppCubit(this.prefs) : super(InitAppState());

  late SharedPreferences prefs;

  static AppCubit get(BuildContext context) => BlocProvider.of(context);

  List<ImageModel> galleryList = [];

  late int activeScreenIndex;
  late ScreenModel activeScreen;
  late ThemeMode themeMode;
  late bool readingMode;
  late UserModel user;
  late String? country;
  late bool? gender;
  late DateTime? birthDate;
  late String? profileImage;
  XFile? pickedProfileImage;
  late bool profileInfoEditMode;

  void initApp() {
    getGallery();
    profileInfoEditMode = false;
    changeActiveScreenIndex(0);
    int? tm = prefs.getInt("themeMode");
    themeMode = tm != null ? ThemeMode.values[tm] : ThemeMode.light;
    readingMode = prefs.getBool("readingMode") ?? true;
    pickedProfileImage = null;
    implementReadingMode();

    String? userAsString = prefs.getString("user");
    if (userAsString != null) {
      user = UserModel.fromJson(jsonDecode(userAsString));
      fillUserInfoFromUserObject();
    }

    emit(InitAppState());
  }

  void fillUserInfoFromUserObject() {
    country = user.country;
    gender = user.gender;
    birthDate = user.birthdate != null ? DateTime.parse(user.birthdate!) : null;
    profileImage = user.image;
  }

  void fillUserObjectFromUserInfo({String? c, bool? g, String? b, String? p}) {
    user.country = c ?? country;
    user.gender = g ?? gender;
    user.birthdate = b ?? (birthDate != null ? intl.DateFormat("yyyy-MM-dd").format(birthDate!) : null);
    user.image = p ?? profileImage;
  }

  void changeUser(UserModel u) {
    user = u;
    emit(ChangeUserState());
  }

  void changeCountry(String? c) {
    country = c;
    emit(ChangeCountryState());
  }

  void changeGender(bool g) {
    gender = g;
    emit(ChangeGenderState());
  }

  void changeProfileInfoEditMode(bool val) {
    profileInfoEditMode = val;
    emit(ChangeProfileInfoEditModeState());
  }

  void cancelProfileInfo() async {
    fillUserInfoFromUserObject();
    profileInfoEditMode = false;
    emit(CancelProfileInfoState());
  }

  Future<bool> saveProfileInfo() async {
    if (gender != null && birthDate != null && country != null) {
      emit(SaveProfileInfoStartedState());
      await DioHelper.dio.post("/api/updateUserData", data: {
        "user": user.id,
        "gender": gender! ? "انثى" : "ذكر",
        'birthdate': intl.DateFormat("yyyy-MM-dd").format(birthDate!),
        "country": country,
      });
      profileInfoEditMode = false;
      fillUserObjectFromUserInfo();
      prefs.setString("user", jsonEncode(user.toJson()));
    } else {
      return false;
    }

    emit(SaveProfileInfoFinishedState());
    return true;
  }

  void changePickedImageProfile(XFile? v) {
    pickedProfileImage = v;
    emit(ChangePickedImageProfileState());
  }

  void saveImageProfile() async {
    emit(SaveImageProfileStartedState());
    String? fileName = pickedProfileImage != null ? pickedProfileImage!.path.split('/').last : null;
    Response? res;
    if (fileName != null) {
      FormData formData = FormData.fromMap({
        "user": user.id,
        "profileImage": await MultipartFile.fromFile(pickedProfileImage!.path, filename: fileName),
      });
      res = await DioHelper.dio.post("/api/updateUserData", data: formData);
      profileImage = res.data["profileImage"];
    } else {
      res = await DioHelper.dio.post("/api/deleteProfileImage");
      profileImage = "";
    }
    profileInfoEditMode = false;
    fillUserObjectFromUserInfo();
    prefs.setString("user", jsonEncode(user.toJson()));

    emit(SaveImageProfileFinishedState());
  }

  void changeBirthDate(DateTime? d) {
    birthDate = d;
    emit(ChangeBirthDateState());
  }

  void changeActiveScreenIndex(int index) {
    activeScreenIndex = index;
    activeScreen = Constants.screens[index];
    emit(ChangeActiveScreenIndexState());
  }

  void changeThemeMode() {
    themeMode = themeMode == ThemeMode.dark ? ThemeMode.light : ThemeMode.dark;
    prefs.setInt("themeMode", themeMode.index);
    emit(ChangeThemeModeState());
  }

  void changeReadingMode() {
    readingMode = !readingMode;
    prefs.setBool("readingMode", readingMode);
    implementReadingMode();
    emit(ChangeReadingModeState());
  }

  void implementReadingMode() {
    if (readingMode) {
      Wakelock.enable();
    } else {
      Wakelock.disable();
    }
  }

  // Start Gallery

  TextEditingController galleryNameControllerObj = TextEditingController();
  bool galleryIsCarouselObj = false;
  XFile? gallerySelectedImageObj;
  CubitMode galleryCubitMode = CubitMode.create;

  void galleryBeginCreateObj() {
    galleryNameControllerObj.clear();
    galleryIsCarouselObj = false;
    gallerySelectedImageObj = null;
    galleryCubitMode = CubitMode.create;
    emit(GalleryBeginCreateObjState());
  }

  void changeSelectedImageObj(XFile? v) {
    gallerySelectedImageObj = v;
    emit(ChangeSelectedImageObjState());
  }

  void changeGalleryIsCarouselObj(bool v) {
    galleryIsCarouselObj = v;
    emit(ChangeGalleryIsCarouselObjState());
  }

  Future<bool> saveGalleryObj() async {
    emit(SaveGalleryObjStartedState());
    bool resault = false;
    if (galleryCubitMode == CubitMode.create) {
      String fileName = gallerySelectedImageObj!.path.split('/').last;
      FormData formData = FormData.fromMap({
        "name": galleryNameControllerObj.text,
        "image": await MultipartFile.fromFile(gallerySelectedImageObj!.path, filename: fileName),
        "isCarousel": galleryIsCarouselObj,
      });
      var res = await DioHelper.dio.post("/api/add_photo", data: formData);
      if (res.statusCode == 201) {
        galleryList.add(ImageModel.fromJson(res.data));
        resault = true;
      }
    }
    emit(SaveGalleryObjFinishedState());
    return resault;
  }

  Future<void> getGallery() async {
    emit(GetGalleryStartedState());
    galleryList = [];
    var res = await DioHelper.dio.get('/api/all_photos');
    for (var item in res.data["data"]) {
      galleryList.add(ImageModel.fromJson(item));
    }
    emit(GetGalleryFinishedState());
  }

  // End Gallery

}
