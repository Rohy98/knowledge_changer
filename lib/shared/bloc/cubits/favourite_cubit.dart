import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/models/featured_text_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/favourite_states.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class FavouriteCubit extends Cubit<FavouriteStates> {
  FavouriteCubit(this.appCubit) : super(InitFavouriteState());
  final AppCubit appCubit;

  List<int> favoriteArticlesList = [];
  List<int> favoriteCoursesList = [];
  bool getFavouriteLoading = false;
  bool getHighlightLoading = false;

  static FavouriteCubit get(BuildContext context) => BlocProvider.of(context);

  void initFavouriteCubit() async {
    await getFavourite();
    await getHighLight();
    emit(InitFavouriteState());
  }

  void changeFavouriteLoading(bool value) {
    getFavouriteLoading = value;
    emit(ChangeGetFavouriteLoadingState());
  }

  void changeHighlightLoading(bool value) {
    getHighlightLoading = value;
    emit(ChangeGetHighlightLoadingState());
  }

  Future<void> getFavourite([bool withLoading = true]) async {
    if (withLoading) {
      emit(GetFavouriteStartedState());
      changeFavouriteLoading(true);
    }
    var favouriteRes = await DioHelper.dio.get('/api/get_favourites');
    favoriteArticlesList = [];
    favoriteCoursesList = [];
    for (var item in favouriteRes.data["data"]) {
      if (item["course_id"] != null) {
        favoriteCoursesList.add(item["course_id"]);
      } else {
        favoriteArticlesList.add(item["article_id"]);
      }
    }
    if (withLoading) {
      changeFavouriteLoading(false);
    }
    emit(GetFavouriteFinishedState());
  }

  Future<void> addToFavourite(String type, int id) async {
    emit(AddToFavouriteStartedState());
    await DioHelper.dio.post('/api/add_favourite/$type/$id/');
    if (type == "course") {
      favoriteCoursesList.add(id);
    } else {
      favoriteArticlesList.add(id);
    }
    emit(AddToFavouriteFinishedState());
  }

  Future<void> deleteFromFavourite(String type, int id) async {
    emit(DeleteFromFavouriteStartedState());
    var res = await DioHelper.dio.post('/api/delete_favourite/$type/$id/');
    if (res.statusCode == 200) {
      if (type == "course") {
        favoriteCoursesList.remove(id);
      } else {
        favoriteArticlesList.remove(id);
      }
    }
    emit(DeleteFromFavouriteFinishedState());
  }

  ///////////////////////////////////////////////

  List<FeaturedTextModel> featuredTextList = [];

  Future<void> getHighLight([bool withLoading = true]) async {
    if (withLoading) {
      emit(GetHighlightStartedState());
      changeFavouriteLoading(true);
    }

    var highlightRes = await DioHelper.dio.get('/api/get_highlight');
    featuredTextList = [];
    for (var item in highlightRes.data) {
      featuredTextList.add(FeaturedTextModel.fromJson(item));
    }

    if (withLoading) {
      changeFavouriteLoading(false);
    }
    emit(GetHighlightFinishedState());
  }

  Future<void> addHighlight(FeaturedTextModel obj) async {
    emit(AddHighlightStartedState());
    var res = await DioHelper.dio.post(
      '/api/add_highlight',
      data: obj.toJson(),
    );
    if (res.statusCode == 200) {
      featuredTextList.add(FeaturedTextModel.fromJson(res.data));
    }
    emit(AddHighlightFinishedState());
  }

  Future<void> deleteHighlight(int id) async {
    var res = await DioHelper.dio.post('/api/delete_highlight/$id/');
    if (res.statusCode == 200 && res.data == "Success") {
      featuredTextList.removeWhere((element) => element.id == id);
    }
    emit(DeleteHighlightFinishedState());
  }
}
