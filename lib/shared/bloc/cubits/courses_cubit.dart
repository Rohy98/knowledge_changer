import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/models/category_model.dart';
import 'package:knowledge_changer/models/course_model.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/courses_states.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class CoursesCubit extends Cubit<CoursesStates> {
  CoursesCubit(this.appCubit) : super(InitCoursesCubitState());

  final AppCubit appCubit;

  // List Start

  List<CourseModel> coursesList = [];
  List<CategoryModel> coursesCategoriesList = [];

  static CoursesCubit get(BuildContext context) => BlocProvider.of(context);

  void initCoursesCubit() async {
    await getCoursesCategories();
    await getCourses();
    emit(InitCoursesCubitState());
  }

  Future<void> getCourses([bool withLoading = true]) async {
    if (withLoading) emit(GetCoursesStartedState());
    var res = await DioHelper.dio.get('/api/courses');
    coursesList = [];
    for (var item in res.data) {
      var a = CourseModel.fromJson(item);
      a.image = a.imageId != null ? appCubit.galleryList.firstWhere((element) => element.id == a.imageId) : null;
      coursesList.add(a);
    }
    emit(GetCoursesFinishedState());
  }

  Future<void> getCoursesCategories([bool withLoading = true]) async {
    if (withLoading) emit(GetCoursesCategoriesStartedState());
    var res = await DioHelper.dio.get('/api/courses_categories');
    coursesCategoriesList = [];
    for (var item in res.data) {
      coursesCategoriesList.add(CategoryModel.fromJson(item));
    }
    emit(GetCoursesCategoriesFinishedState());
  }
//   void changeFavoriteList(ArticleModel article) {
//     bool isExist = false;
//     favoriteArticlesList.firstWhere((element) {
//       if (element.id == article.id) isExist = true;
//       return element.id == article.id;
//     }, orElse: () => ArticleModel(id: -1, title: "", intro: "", imageId: -1, body: "", date: DateTime.now(), categoryId: -1));
//     if (isExist) {
//       favoriteArticlesList.remove(article);
//     } else {
//       favoriteArticlesList.add(article);
//     }
//     emit(ChangeFavoriteListState());
//   }

//   // List End

//   // Add And Edit Start

//   TextEditingController titleController = TextEditingController();
//   TextEditingController writerController = TextEditingController();
//   TextEditingController introController = TextEditingController();
//   TextEditingController bodyController = TextEditingController();
//   DateTime? date;
//   ImageModel? image;
//   CategoryModel? category;
//   CubitMode cubitMode = CubitMode.create;
//   ArticleModel? articleForEdit;

//   void beginCreate(CategoryModel? c) {
//     titleController.clear();
//     introController.clear();
//     bodyController.clear();
//     writerController.clear();
//     image = null;
//     date = DateTime.now();
//     category = c;
//     cubitMode = CubitMode.create;
//   }

//   void beginEdit(ArticleModel a) {
//     articleForEdit = a;
//     titleController.text = a.title;
//     introController.text = a.intro;
//     bodyController.text = a.body;
//     writerController.text = a.writtenBy ?? "";
//     image = a.image;
//     date = a.date;
//     category = categoriesCubit.categoriesList.firstWhereOrNull((element) => element.id == a.categoryId);
//     cubitMode = CubitMode.edit;
//   }

//   Future<bool> saveArticle() async {
//     emit(SaveArticleStartedState());
//     bool result = false;
//     if (category != null) {
//       var formData = FormData.fromMap({
//         "title": titleController.text,
//         "written_by": writerController.text,
//         "intro": introController.text,
//         "image": image!.id,
//         "body": bodyController.text,
//         "date": date,
//         "category": category!.id,
//       });
//       var res = await DioHelper.dio.post(cubitMode == CubitMode.create ? '/api/create_article' : '/api/update_article/${articleForEdit!.id}/', data: formData);
//       if (res.statusCode == 200) {
//         ArticleModel obj = ArticleModel.fromJson(res.data);
//         obj.image = appCubit.galleryList.firstWhere((element) => element.id == obj.imageId);
//         if (cubitMode == CubitMode.create) {
//           articlesList.add(obj);
//         } else {
//           ArticleModel? a = articlesList.firstWhereOrNull((element) => element.id == obj.id);
//           if (a != null) {
//             a.title = obj.title;
//             a.body = obj.body;
//             a.intro = obj.intro;
//             a.categoryId = obj.categoryId;
//             a.date = obj.date;
//             a.imageId = obj.imageId;
//             a.image = obj.image;
//             a.writtenBy = obj.writtenBy;
//           }
//         }

//         result = true;
//       }
//     }
//     emit(SaveArticleFinishedState());
//     return result;
//   }

//   void changeCategory(CategoryModel? c) {
//     category = c;
//     emit(ChangeCategoryState());
//   }

//   void changeImage(ImageModel? i) {
//     image = i;
//     emit(ChangeImageState());
//   }

//   // Add And Edit End

//   // Start Delete

//   Future<void> delete(ArticleModel article) async {
//     var res = await DioHelper.dio.post('/api/delete_article/${article.id}/');
//     if (res.statusCode == 200) {
//       articlesList.remove(article);
//     }
//     emit(DeleteArticleState());
//   }

//   // End Delete
}
