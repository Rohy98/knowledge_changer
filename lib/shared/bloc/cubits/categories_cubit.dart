import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:knowledge_changer/models/category_model.dart';
import 'package:knowledge_changer/shared/bloc/states/categories_states.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';

class CategoriesCubit extends Cubit<CategoriesStates> {
  CategoriesCubit() : super(InitCategoriesCubitState());

  late List<CategoryModel> categoriesList;

  static CategoriesCubit get(BuildContext context) => BlocProvider.of(context);

  void initCategoriesCubit() async {
    await getCategories();
    emit(InitCategoriesCubitState());
  }

  Future<void> getCategories() async {
    emit(GetCategoriesStartedState());
    categoriesList = [];
    var res = await DioHelper.dio.get('/api/categories');
    for (var item in res.data) {
      categoriesList.add(CategoryModel.fromJson(item));
    }
    emit(GetCategoriesFinishedState());
  }

  Future addCategory(CategoryModel c) async {
    emit(AddCategoryState());
  }

  Future removeCategory(CategoryModel c) async {
    emit(RemoveCategoryState());
  }
}
