class AppStates {}

class InitAppState extends AppStates {}

class ChangeActiveScreenIndexState extends AppStates {}

class ChangeThemeModeState extends AppStates {}

class ChangeReadingModeState extends AppStates {}

class GetGalleryStartedState extends AppStates {}

class GetGalleryFinishedState extends AppStates {}

class GalleryBeginCreateObjState extends AppStates {}

class ChangeSelectedImageObjState extends AppStates {}

class ChangeGalleryIsCarouselObjState extends AppStates {}

class SaveGalleryObjStartedState extends AppStates {}

class SaveGalleryObjFinishedState extends AppStates {}

class ChangeUserState extends AppStates {}

class ChangeCountryState extends AppStates {}

class ChangeGenderState extends AppStates {}

class ChangeBirthDateState extends AppStates {}

class ChangeProfileInfoEditModeState extends AppStates {}

class CancelProfileInfoState extends AppStates {}

class SaveProfileInfoStartedState extends AppStates {}

class SaveProfileInfoFinishedState extends AppStates {}

class ChangePickedImageProfileState extends AppStates {}

class SaveImageProfileStartedState extends AppStates {}

class SaveImageProfileFinishedState extends AppStates {}
