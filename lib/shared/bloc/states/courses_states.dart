class CoursesStates {}

class InitCoursesCubitState extends CoursesStates {}

class GetCoursesStartedState extends CoursesStates {}

class GetCoursesFinishedState extends CoursesStates {}

class SaveCoursestartedState extends CoursesStates {}

class SaveCoursesFinishedState extends CoursesStates {}

class ChangeFavoriteListState extends CoursesStates {}

class InitCoursesListStartedState extends CoursesStates {}

class InitCoursesListState extends CoursesStates {}

class AddCoursesState extends CoursesStates {}

class ChangeCategoryState extends CoursesStates {}

class ChangeImageState extends CoursesStates {}

class DeleteCoursestate extends CoursesStates {}

class GetCoursesCategoriesStartedState extends CoursesStates {}

class GetCoursesCategoriesFinishedState extends CoursesStates {}
