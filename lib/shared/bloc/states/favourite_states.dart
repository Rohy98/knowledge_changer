class FavouriteStates {}

class InitFavouriteState extends FavouriteStates {}

class GetFavouriteStartedState extends FavouriteStates {}

class GetFavouriteFinishedState extends FavouriteStates {}

class GetHighlightStartedState extends FavouriteStates {}

class GetHighlightFinishedState extends FavouriteStates {}

class AddHighlightStartedState extends FavouriteStates {}

class AddHighlightFinishedState extends FavouriteStates {}

class DeleteFromFavouriteStartedState extends FavouriteStates {}

class DeleteFromFavouriteFinishedState extends FavouriteStates {}

class AddToFavouriteStartedState extends FavouriteStates {}

class AddToFavouriteFinishedState extends FavouriteStates {}

class ChangeGetFavouriteLoadingState extends FavouriteStates {}

class ChangeGetHighlightLoadingState extends FavouriteStates {}

class DeleteHighlightFinishedState extends FavouriteStates {}
