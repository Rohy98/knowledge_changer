class ArticlesStates {}

class InitArticlesCubitState extends ArticlesStates {}

class GetArticlesStartedState extends ArticlesStates {}

class GetArticlesFinishedState extends ArticlesStates {}

class SaveArticleStartedState extends ArticlesStates {}

class SaveArticleFinishedState extends ArticlesStates {}

class ChangeFavoriteListState extends ArticlesStates {}

class InitArticlesListStartedState extends ArticlesStates {}

class InitArticlesListState extends ArticlesStates {}

class AddArticlesState extends ArticlesStates {}

class ChangeCategoryState extends ArticlesStates {}

class ChangeImageState extends ArticlesStates {}

class DeleteArticleState extends ArticlesStates {}

class ChangeSelectedCategoryArticlesListState extends ArticlesStates {}

class ChangeSearchTextState extends ArticlesStates {}

class ChangeLoadingState extends ArticlesStates {}

class DeleteFromFeaturedTextListState extends ArticlesStates {}

class AddToFeaturedTextListState extends ArticlesStates {}

class GetFeaturedTextListState extends ArticlesStates {}
