class CategoriesStates {}

class InitCategoriesCubitState extends CategoriesStates {}

class InitCategoriesListState extends CategoriesStates {}

class AddCategoryState extends CategoriesStates {}

class RemoveCategoryState extends CategoriesStates {}

class GetCategoriesStartedState extends CategoriesStates {}

class GetCategoriesFinishedState extends CategoriesStates {}
