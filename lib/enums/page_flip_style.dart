enum PageFlipStyle {
  horizontal,
  vertical,
}

abstract class PageFlipStyleLocalization {
  static List<String> l = [
    "أفقي",
    "عامودي",
  ];

  static String tr(PageFlipStyle e) {
    return l[e.index];
  }
}
