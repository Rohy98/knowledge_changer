import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_sell_sdk_flutter/go_sell_sdk_flutter.dart';
import 'package:knowledge_changer/components/network_aware_widget.dart';
import 'package:knowledge_changer/components/utils/buttons_components.dart';
import 'package:knowledge_changer/components/utils/utils_components.dart';
import 'package:knowledge_changer/modules/auth/login_page.dart';
import 'package:knowledge_changer/modules/auth/register_page.dart';
import 'package:knowledge_changer/modules/shared/shared_layout.dart';
import 'package:knowledge_changer/shared/bloc/cubits/app_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/articles_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/categories_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/courses_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/favourite_cubit.dart';
import 'package:knowledge_changer/shared/bloc/cubits/reviews_cubit.dart';
import 'package:knowledge_changer/shared/bloc/states/app_states.dart';
import 'package:knowledge_changer/shared/utils/dio_helper.dart';
import 'package:knowledge_changer/shared/utils/network_status_service.dart';
import 'package:knowledge_changer/shared/utils/payment_handler.dart';
import 'package:knowledge_changer/shared/utils/shared_utils.dart';
import 'package:knowledge_changer/styles/themes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';

Future<void> configureApp() async {
  GoSellSdkFlutter.configureApp(
    bundleId: "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ",
    productionSecreteKey: Platform.isAndroid ? "Android-Live-Key" : "iOS-Live-Key",
    sandBoxsecretKey: Platform.isAndroid ? "Android-SANDBOX-KEY" : "iOS-SANDBOX-KEY",
    lang: "ar",
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString("token");
  if (token != null && token != "") {
    DioHelper.init(token);
  } else {
    DioHelper.init();
  }

  configureApp();
  PaymentHandler.setupSDKSession();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AppCubit>(
          create: (BuildContext context) => AppCubit(prefs)..initApp(),
        ),
        BlocProvider<CategoriesCubit>(
          create: (BuildContext context) => CategoriesCubit()..initCategoriesCubit(),
        ),
        BlocProvider<ArticlesCubit>(
          create: (BuildContext context) => ArticlesCubit(AppCubit.get(context), CategoriesCubit.get(context))..initArticlesCubit(),
        ),
        BlocProvider<CoursesCubit>(
          create: (BuildContext context) => CoursesCubit(AppCubit.get(context))..initCoursesCubit(),
        ),
        BlocProvider<FavouriteCubit>(
          create: (BuildContext context) => FavouriteCubit(AppCubit.get(context))..initFavouriteCubit(),
        ),
        BlocProvider<ReviewsCubit>(
          create: (BuildContext context) => ReviewsCubit()..initReviewsCubit(),
        ),
      ],
      child: MyApp(
        prefs: prefs,
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required this.prefs,
  }) : super(key: key);

  final SharedPreferences prefs;

  @override
  Widget build(BuildContext context) {
    String? userAsString = prefs.getString("user");
    String? username;
    bool hasAccount = false;
    if (userAsString != null) {
      hasAccount = true;
      username = jsonDecode(userAsString)["username"];
    }
    String? token = prefs.getString("token");
    bool goToLogin = token == null || token == "";
    return FutureBuilder<ConnectivityResult>(
      future: Connectivity().checkConnectivity(),
      builder: (context, AsyncSnapshot<ConnectivityResult> snapshot) {
        return StreamProvider<NetworkStatus>(
          initialData: snapshot.hasData ? SharedUtils.getNetworkStatus(snapshot.data!) : NetworkStatus.offline,
          create: (context) => NetworkStatusService().networkStatusController.stream,
          child: BlocConsumer<AppCubit, AppStates>(
            listener: (context, states) {},
            builder: (context, states) {
              AppCubit appCubit = AppCubit.get(context);
              return MaterialApp(
                title: 'Knowledge Changer',
                debugShowCheckedModeBanner: false,
                theme: Themes.lightTheme,
                darkTheme: Themes.darkTheme,
                themeMode: appCubit.themeMode,
                home: Directionality(
                  textDirection: TextDirection.rtl,
                  child: NetworkAwareWidget(
                    onlineChild: !hasAccount
                        ? const RegisterPage()
                        : goToLogin
                            ? LoginPage(
                                username: username,
                              )
                            : const SharedLayout(),
                    offlineChild: Scaffold(
                      body: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const IconAndText(
                            text: "لا يوجد إتصال بالإنترنت",
                            icon: Icons.signal_wifi_statusbar_connected_no_internet_4,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          TextIconMaterialButton(
                            label: "إعادة المحاولة",
                            icon: Icons.refresh,
                            color: Theme.of(context).colorScheme.primary,
                            onTap: () async {
                              NetworkStatusService().networkStatusController.add(SharedUtils.getNetworkStatus(await Connectivity().checkConnectivity()));
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                builder: (context, widget) {
                  return ScrollConfiguration(behavior: const ScrollBehaviorModified(), child: widget!);
                },
              );
            },
          ),
        );
      },
    );
  }
}

class ScrollBehaviorModified extends ScrollBehavior {
  const ScrollBehaviorModified();
  @override
  ScrollPhysics getScrollPhysics(BuildContext context) {
    switch (getPlatform(context)) {
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
      case TargetPlatform.android:
        return const BouncingScrollPhysics();
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return const ClampingScrollPhysics();
    }
  }
}
